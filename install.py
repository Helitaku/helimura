import os
import sys
import argparse
import shutil
import tempfile
import logging
from zipfile import ZipFile
from pathlib import Path

import PyInstaller.__main__

from helimura.core.project import FileManager
from helimura.core import runtime
from helimura.core.apis import node

from helimura.ui.gui.app.backend import Page


class Compiler(object):
    bundle_name = runtime.program_name
    binary_name = runtime.binary_name
    root_directory = os.path.dirname(__file__)
    runtime_document_list = runtime.program_document_list
    package_directory = os.path.join(root_directory, bundle_name)
    config_path = FileManager.project_config_path

    archive_name = bundle_name + ".zip"
    separator = ":" if os.sys.platform == "linux" else ";"  # os.path.sep

    def compile(target_dir, include=[], onefile=False):

        pyinstall_dir = os.path.join(target_dir, ".pyinstall")
        cmd = [
            os.path.join(Compiler.package_directory, "__main__.py"),
            "--name",
            Compiler.bundle_name,
            "--onefile" if onefile else "",
            "--clean",
            "--distpath",
            target_dir,
            "--workpath",
            pyinstall_dir,
        ]
        for src_d, dest_d in include:
            cmd += [
                "--add-data",
                f"{src_d}{Compiler.separator}{dest_d}",
            ]
        try:
            PyInstaller.__main__.run(cmd)
        except Exception:
            raise
        else:
            shutil.rmtree(pyinstall_dir)
        # shutil.remove(fw)


def bundle_it(source_directory, target_directory, include_node=False, extra_files=[]):
    package_parent_directory = runtime.package_parent_directory
    bundle_list = [
        (os.path.join(package_parent_directory, src), dest) for src, dest in extra_files
    ]
    Compiler.compile(
        target_directory,
        include=bundle_list,
        onefile=True,
    )


def zip_it(src_directory, target_directory):
    with ZipFile(os.path.join(target_directory, Compiler.archive_name), "w") as zf:
        buf = ""
        for r, d, files in os.walk(src_directory):
            for fi in files:
                src_path = Path(r) / Path(fi)
                zf_path = src_path.relative_to(src_directory)
                zf.write(src_path, zf_path)
                buf += "%s\n" % zf_path

        zf.writestr("content.txt", buf.encode("ASCII"))


def install(target_directory, include_node=False, format="binary"):

    src_root_directory = Compiler.root_directory
    os.makedirs(target_directory, exist_ok=True)

    if format == "binary":
        bundle_it(
            src_root_directory,
            target_directory,
            include_node,
            extra_files=Compiler.runtime_document_list,
        )
    elif format == "archive":
        with tempfile.TemporaryDirectory() as tmp_dir:
            bundle_it(src_root_directory, tmp_dir)
            for src_path, dest_path in Compiler.runtime_document_list:
                src = Path(src_root_directory) / src_path
                dest = Path(tmp_dir) / dest_path
                if not src.is_file():
                    shutil.copytree(src, dest, dirs_exist_ok=True)
                else:
                    shutil.copy(src, dest)
            zip_it(
                tmp_dir,
                target_directory,
            )


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("destination", help="Target directory for the binary file")
    parser.add_argument(
        "--format",
        help="Output of the installer. Options are 'binary' (PyInstaller executable), or 'archive' (ZIP archive). Defaults to archive.",
        default="archive",
    )
    parser.add_argument(
        "--include_node", action="store_true", help="Embed optional node binary"
    )

    args = parser.parse_args()
    install(args.destination, include_node=args.include_node, format=args.format)
