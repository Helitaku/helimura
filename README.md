# Helimura

<img src="./media/spot.png" width="400"/>

Helimura is a framework for showcasing digital content exclusively from specific areas in the world.   
This allows for exclusive artworks, with topics highly relevant to their location.  
The artworks live on the decentralized network [IPFS](https://docs.ipfs.tech/concepts/what-is-ipfs/), thus anybody can contribute to the hosting of their favorites.  

For more info  
[https://helitaku.codeberg.page/helimura.html](https://helitaku.codeberg.page/helimura.html)
# Requirements
To fully use the framework, you need [to run a IPFS node](https://docs.ipfs.tech/concepts/nodes) on your machine, or your local network.  
Install IPFS [with a Desktop app](https://docs.ipfs.tech/install/ipfs-desktop/) or [barebone](https://docs.ipfs.tech/install/command-line/#system-requirements), and launch it before using Helimura.

# Installation
## (Recommended) Download executable
Download [the latest release](https://codeberg.org/helitaku/helimura/releases) for your OS (Linux or Windows), and extract the archive somewhere.
## Pip/Pipenv (Linux)
```
git clone https://codeberg.org/helitaku/helimura
cd helimura
# pip
pip install -r requirements.txt
# pipenv
pipenv install -r requirements.txt
```
## With docker
```
git clone https://codeberg.org/helitaku/helimura
cd helimura
docker build -f docker/Dockerfile -t helimura:latest .
```

# Usage

There are two ways to use Helimura: from a web app, or from a terminal.  
**First time users** can follow a quickstart interactive guide from the web app. 

## Interactive

### Quickstart guide 

With **executable**:
* Navigate to the folder you extracted  
* Double click on the executable *helimura(.exe)*  

With **command line**:  
* Run Helimura without any additional argument from your terminal.

Whichever method you choose, open a web browser at the address [http://127.0.0.1:3000/tutorial/quickstart](http://127.0.0.1:3000/tutorial/quickstart), and follow the instructions

### Project building via the web app

Those who completed the tutorial know how to publish their work to IPFS. They can do it via the web app, with the project builder.

With **executable**:  
* Navigate to the folder you extracted  
* Double click on the executable *helimura(.exe)*  

With **command line**:  
* Run Helimura with additional flag *setup*

Whichever method you choose, open a web browser at the address [http://127.0.0.1:3000](http://127.0.0.1:3000)

## Command line

### Python

`python -m helimura --help`

### Pipenv

`pipenv run python -m helimura --help`

### Docker

```
# network=host is for the program to communicate with a IPFS node on your host machine,
# and make the webui available at port 3000 for accessing the web ui
docker run --rm -ti -v helimura_data:/root/helimura/projects --network=host helimura:latest helimura --help
```

# About us
[https://helitaku.codeberg.page](https://helitaku.codeberg.page)
