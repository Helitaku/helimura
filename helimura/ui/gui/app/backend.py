import os
import sys
import uuid
import markdown
import logging
import traceback
from io import StringIO
from helimura.core import project, runtime
import helimura.ui.tui.parser as parser


default_project_name = default_project_name = (
    "project-" + str(uuid.uuid4()).replace("-", "")[:5]
)


# Set the default project directory in the home folder of the user
default_project_directory = project.FileManager.default_project_directory
# Alternatively, we may want to set the project directory in the same folder as the executable instead
# if module.Path.is_pyinstaller_bundle:
#     directory = os.path.join(
#         os.path.dirname(module.Path.program_file), "projects", name
#     )
# else:
#     directory = os.path.join(
#         os.path.abspath(os.path.dirname(os.path.dirname(module.__file__))),
#         "projects",
#         name,
#     )

pages_root = os.path.join(runtime.pages_directory)


class Project(object):
    path = project.FileManager
    name = path.default_project_name
    directory = os.path.join(default_project_directory, name)

    @staticmethod
    def set(path):
        if not os.path.exists(path):
            raise OSError(path + "\nProject doesn't exist")
        Project.name = project.as_config(path).load().get("name")
        Project.directory = path


class Page(object):
    setup_path = os.path.join(pages_root, "setup")
    guide_path = os.path.join(pages_root, "guide")
    template_path = os.path.join(pages_root, "templates")

    class _Get(object):
        @staticmethod
        def from_template(file_name):
            with open(os.path.join(Page.template_path, file_name)) as f:
                return f.read()

        @staticmethod
        def from_markdown(filename):
            markdown_page = filename
            with open(markdown_page, "r") as f:
                result = f"<html><body>{markdown.markdown(f.read(), extensions=['md_in_html'])}</body></html>"
            return result

        @staticmethod
        def get_default_html(content=[]):
            return (
                "<!DOCTYPE html>"
                + '<html lang="en">'
                + "<head>"
                + '  <meta charset="UTF-8">'
                + '  <meta name="viewport" content="width=device-width, initial-scale=1.0">'
                + '  <meta http-equiv="X-UA-Compatible" content="ie=edge">'
                + "  <title>Home</title>"
                + "</head>"
                + "<body>"
                + "\n".join(content)
                + "</body>"
                + "</html>"
            )

        @staticmethod
        def project_header():
            return Page._Get.from_template("header.html")

        @staticmethod
        def request():
            return Page._Get.from_template("request.html")

        @staticmethod
        def get_main():
            data = {
                "path": Project.path.__dict__,
                "directory": Project.directory,
                "name": Project.name,
            }
            return (
                Page._Get.get_default_html(
                    [
                        Page._Get.from_template("setup.html"),
                    ]
                ),
                data,
            )

        @staticmethod
        def get_info():
            config = project.as_config(Project.directory)
            data = {
                "text": str(config),
                "directory": Project.directory,
                "name": Project.name,
            }
            return (
                Page._Get.get_default_html(
                    [
                        Page._Get.from_template("info.html"),
                    ]
                ),
                data,
            )

        @staticmethod
        def get_guide(filename=""):
            p = Project.path(Project.directory)
            ipfs_node = str(p.ipfs_node)
            ipfs_gateway = str(p.ipfs_gateway)
            return Page._Get.from_markdown(os.path.join(Page.guide_path, filename)), {
                "path": project.FileManager.__dict__,
                "ipfs_node": ipfs_node,
                "ipfs_gateway": ipfs_gateway,
                "directory": Project.directory,
                "name": Project.name,
            }

        @staticmethod
        def get_new():
            data = {
                "directory": os.path.join(
                    default_project_directory, default_project_name
                ),
                "name": default_project_name,
                "templates": parser.parse_arguments_defaults["init"]["parameters"][
                    "template"
                ]["choices"],
            }
            return (
                Page._Get.get_default_html([Page._Get.from_template("new.html")]),
                data,
            )

        @staticmethod
        def get_link():
            gw = Project.path(Project.directory).ipfs_gateway
            data = {
                "path": Project.path.__dict__,
                "directory": Project.directory,
                "name": Project.name,
                "src_gateway": os.getenv("PUBLISH_URL") or gw,
                "dest_gateway": os.getenv("DEFAULT_ACCESS_URL") or gw,
            }
            return (
                Page._Get.get_default_html(
                    [
                        Page._Get.from_template("link.html"),
                    ]
                ),
                data,
            )

        @staticmethod
        def get_undefined():
            return (
                Page._Get.get_default_html([Page._Get.from_template("undefined.html")]),
                {
                    "directory": Project.directory,
                    "config_file": project.ProjectConfig.default_file_name,
                },
            )

        @staticmethod
        def get_geojson():
            data = {
                "directory": Project.directory,
                "name": Project.name,
            }
            return (
                Page._Get.get_default_html(
                    [
                        Page._Get.from_template("geojson.html"),
                    ]
                ),
                data,
            )

        @staticmethod
        def get_release():
            data = {
                "directory": Project.directory,
                "name": Project.name,
            }
            p = Project.path(Project.directory)
            config = p.config.load()
            data |= {"config": str(config)}
            return (
                Page._Get.get_default_html(
                    [
                        Page._Get.from_template("release.html"),
                    ]
                ),
                data,
            )

        @staticmethod
        def get_publish():
            p = Project.path(Project.directory)
            data = {
                "directory": Project.directory,
                "name": Project.name,
                "path": Project.path.__dict__,
                "ipfs_node": p.ipfs_node,
                "config": str(p.config),
            }
            return (
                Page._Get.get_default_html(
                    [
                        Page._Get.from_template("publish.html"),
                    ]
                ),
                data,
            )

    class _Post(object):
        @staticmethod
        def _capture_output(command, args):
            args |= {"project_directory": Project.directory}
            old_stdout = sys.stdout
            result = StringIO()
            sys.stdout = result
            try:
                parser.parse_command(command, args)
                result_string = result.getvalue()
            finally:
                sys.stdout = old_stdout
            return result_string

        @staticmethod
        def post_chroot(value):
            Project.set(value["path"])

        @staticmethod
        def post_init(value):
            old = Project.directory
            Project.directory = value["root"]
            try:
                output = Page._Post._capture_output("init", value)
            except Exception as e:
                Project.directory = old
                raise (e)
            else:
                Project.set(value["root"])
                return output

        @staticmethod
        def post_update(value):
            content = project.as_content(Project.directory)
            c = content.config
            c.overwrite_from_string(value["text"])
            c.load()
            Project.name = c.get("name")
            content.build()

        @staticmethod
        def post_release(value):
            return Page._Post._capture_output("prepublish", {})

    @staticmethod
    def get_content(page, **kwargs):
        fn = "get_" + page
        if fn in Page._Get.__dict__:
            return Page._Get.__dict__[fn](**kwargs)
        else:
            return None, {}

    @staticmethod
    def post_content(request):
        response = {"result": None, "text": "", "output": ""}
        output = ""
        command = request["command"]
        args = request["value"]
        fn = "post_" + command

        log_stream = StringIO()
        logger = logging.getLogger()
        logger.setLevel(logging.INFO)
        ch = logging.StreamHandler(log_stream)
        ch.setLevel(logging.INFO)
        logger.addHandler(ch)
        # logger.addHandler(logging.StreamHandler(stream=sys.__stderr__))
        try:
            if fn in Page._Post.__dict__:
                output = Page._Post.__dict__[fn](args)
            else:
                output = Page._Post._capture_output(command, args)
        except Exception as e:
            err = f"{e}\n\nDetails:\n{''.join(traceback.format_exception(None, e, e.__traceback__))}"
            logging.error(err)
            response = {"result": "failure", "text": str(err)}
            # log_stream.close()
        else:
            response = {
                "result": "success",
                "text": log_stream.getvalue(),
                "output": output,
            }
        return response
