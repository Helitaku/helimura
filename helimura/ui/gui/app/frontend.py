import os
import tempfile
from flask import (
    Blueprint,
    Flask,
    render_template_string,
    send_from_directory,
    send_file,
    redirect,
    render_template,
    url_for,
    request,
    abort,
)
import argparse
import markdown
import logging
import traceback
from jinja2.exceptions import TemplateNotFound
from helimura.core import project, runtime
import helimura.ui.gui.parser as parser

from . import backend
from .backend import Project

app = Flask(__name__, root_path=backend.pages_root, template_folder="templates")


class Frontend(object):
    @app.route("/")
    def set_page():
        if not Project.directory:
            return redirect("/setup/undefined")
        elif not os.path.exists(Project.directory):
            return redirect("/setup/undefined")
        else:
            return redirect("/setup/main")

    @app.errorhandler(500)
    def internal_server_error(e):
        return render_template("500.html", error={"message": str(e)}), 500


class Guide(object):
    guide_bp = Blueprint(
        "tutorial",
        __name__,
        url_prefix="/tutorial",
        template_folder="templates",
        static_folder="static",
    )

    @guide_bp.route("/<page>")
    def get_page(page=""):
        if page == "undefined":
            return redirect("../setup/undefined")
        content, data = backend.Page.get_content("guide", filename=page + ".md")
        if content == None:
            abort(404)
        return render_template_string(content, data=data)

    @staticmethod
    def get_blueprint(app):
        Guide.guide_bp.root_path = os.path.join(app.root_path, "tutorial")
        Guide.guide_bp.app = app
        return Guide.guide_bp


class Setup(object):
    setup_bp = Blueprint(
        "setup",
        __name__,
        url_prefix="/setup",
        template_folder="templates",
        static_folder="static",
    )

    @staticmethod
    def render_page(page):
        content, data = backend.Page.get_content(page)
        if content == None:
            abort(404)
        return render_template_string(content, data=data)

    @setup_bp.route("/post", methods=["POST"])
    def post():
        # Get the JSON data from the request
        data = request.get_json()
        logging.info("REQUEST: " + str(data))
        response = backend.Page.post_content(data)
        return response

    @setup_bp.route("/")
    def redirect_to_main():
        return redirect("/setup/main")

    @setup_bp.route("/<page>")
    def get_setup(page=""):
        if page not in ["undefined", "new"]:
            if not Project.directory:
                return redirect("/setup/undefined")
            elif not os.path.exists(Project.directory):
                return redirect("/setup/undefined")
        return Setup.render_page(page)

    @setup_bp.errorhandler(404)
    def page_not_found(e):
        return render_template("setup404.html"), 404

    @staticmethod
    def get_blueprint(app):
        Setup.setup_bp.root_path = os.path.join(app.root_path, "setup")
        Setup.setup_bp.app = app
        return Setup.setup_bp


class Browse(object):
    content_path = Project.path.content_dirname
    draft_path = Project.path.draft_path
    release_path = Project.path.release_dirname
    geojson_path = Project.path.preview_geojson_path
    geofence_path = Project.path.preview_geofence_path

    browse_bp = Blueprint(
        "browse",
        __name__,
        url_prefix="/browse",
        template_folder="templates",
        static_folder="static",
    )

    @browse_bp.route("/" + content_path + "/")
    def get_content():
        return redirect("..")

    @browse_bp.route("/")
    def get_browse():
        # Show directory contents
        files = [
            Browse.draft_path,
            Browse.release_path,
            Browse.geojson_path,
            Browse.geofence_path,
        ]
        return render_template(
            "files.html",
            root=Project.directory,
            parent="..",
            files=files,
        )

    @browse_bp.route("/<path:req_path>")
    def dir_listing(req_path):
        root = Project.directory
        abs_path = os.path.join(root, req_path)
        # Return 404 if path doesn't exist
        if not os.path.exists(abs_path):
            return abort(404)

        # Check if path is a file and serve
        if os.path.isfile(abs_path):
            return send_file(abs_path)

        # Show directory contents
        files = os.listdir(abs_path)
        return render_template(
            "files.html",
            root=abs_path,
            parent="..",
            files=files,
        )

    @staticmethod
    def get_blueprint(app):
        Browse.browse_bp.root_path = os.path.join(app.root_path, "browse")
        Browse.browse_bp.app = app
        return Browse.browse_bp


class Preview(object):
    draft_path = Project.path.draft_path
    release_path = Project.path.release_dirname

    preview_bp = Blueprint(
        "preview",
        __name__,
        url_prefix="/preview",
        template_folder="templates",
        static_folder="static",
    )

    def download_dir(dir, filename):
        path = os.path.join(dir, filename).replace("\\", "/")
        print(path)
        if not os.path.exists(path):
            abort(404)
        return send_from_directory(
            dir,
            filename,
            as_attachment=False,
        )

    @preview_bp.route("/frame")
    def render_frame():
        return render_template(
            "frame.html", url=Project.path.draft_dist_path.replace("\\", "/")
        )

    @preview_bp.route(f"/content/draft/<path:filename>")
    def download_draft(filename):
        return Preview.download_dir(
            project.ProjectModule(Project.directory).draft_directory, filename
        )

    @preview_bp.route(f"/release")
    def get_release():
        return redirect(f"{Preview.release_path}/")

    @preview_bp.route(f"/release/", defaults={"filename": "index.html"})
    @preview_bp.route(f"/{release_path}/<path:filename>")
    def download_public(filename=""):
        return Preview.download_dir(
            project.ProjectModule(Project.directory).content.release_public_directory,
            filename,
        )

    @preview_bp.errorhandler(404)
    def page_not_found(e):
        return render_template("preview404.html"), 404

    @staticmethod
    def get_blueprint(app):
        Preview.preview_bp.root_path = os.path.join(app.root_path, "preview")
        Preview.preview_bp.app = app
        return Preview.preview_bp


def start(project_directory="", ip="0.0.0.0", port=None):
    if port is None:
        p = (runtime.config.load().get("ui") or {}).get("port")
        port = int(p) if not p is None else 3000
    app.project_name = "Project setup"
    app.register_blueprint(Setup.get_blueprint(app), url_prefix="/setup")
    app.register_blueprint(Guide.get_blueprint(app), url_prefix="/tutorial")
    app.register_blueprint(Browse.get_blueprint(app), url_prefix="/browse")
    app.register_blueprint(Preview.get_blueprint(app), url_prefix="/preview")

    if project_directory:
        if os.path.exists(project_directory):
            Project.set(project_directory)
        else:
            Project.directory = project_directory
    app.run(host=ip, port=port)
