import os
import helimura.parser as mainparser
from helimura.core import runtime
from . import app

parse_arguments_defaults = {
    "setup": {
        "requires_project": False,
        "help": "",
        "parameters": {
            "project": {
                "option_strings": "--project",
                "default": "",
                "help": "Path to the project we want to configure",
            },
            "ip": {
                "option_strings": "--ip",
                "help": "Defaults to 0.0.0.0",
                "default": "0.0.0.0",
            },
            "port": {
                "option_strings": "--port",
                "help": "Open port to reach the setup interface. Defaults to 3000",
                "type": int,
                "default": 3000,
            },
        },
    }
}


def parse_args():
    if not mainparser.get_command() in parse_arguments_defaults:
        # Command is not found in argument parser for this module
        return
    # Use argparse to parse command line arguments
    command, args = mainparser.parse_args(parse_arguments_defaults)
    # runtime.initialize()
    if args:
        # Calls functions related to the parsed arguments
        parse_command(command, args)
    return args


def parse_command(command, args):
    project_directory = None
    if "project" in args:
        if args.project:
            project_directory = os.path.abspath(args.project)
    if command == "setup":
        app.frontend.start(
            project_directory=project_directory, ip=args.ip, port=args.port
        )
