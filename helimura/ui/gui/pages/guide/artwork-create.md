{% include "unified-css.html" %}
[Back to the start of the tutorial](<quickstart#Let's get started>)
# Create your first project
### In the *project creator page*:
* Choose the parent folder of your future project's directory  
* Choose a name for your project  
* Keep the template option as is (*webpage*)
#### Navigate to <a href="../setup/new" >the project creator page</a>

## When you are done with project creation,
#### head to [Project workflow: from creation to publish](artwork-files)
---
### Annexe for advanced users
<details markdown="1">
<summary>Technical information</summary>
Equivalent of command line  

```helimura init webpage [path to your future project directory]```  
</details>
