{% include "unified-css.html" %}
# A quick tutorial to get started

[Back to setup](../setup)
<br>

## To use Helimura, you must have a IPFS node up and running.   
cf. [https://docs.ipfs.tech/install/ipfs-desktop/](https://docs.ipfs.tech/install/ipfs-desktop/)  
<br>
## The content of this tutorial
#### Helimura is made with versatility in mind, but we cooked an example scenario that should fit most cases.   

### You are an artist
<p>Either you are a member of a collective or a single talent, you want to showcases some of your art projects.  </p> 

* One of your project should only be available if its visitor is in your city's public park.  
* Another work is about the history of a monument and should only be visited from the area around the monument.  

### Two step process
* You publish your art project on IPFS, with the details of the areas where it should be visited  
* You (or someone you know) publish a virtual gallery. The gallery will open an <a href="https://www.scientecheasy.com/2024/04/iframe-in-html.html/">IFrame</a> pointing to your project's URL, f
for each of its visitors, if the visitor are in the areas you previously set.    
# Let's get started
## Part 1: artwork
* [Create your first project: artwork](artwork-create)  

* [Project workflow: from creation to publish](artwork-files)  

* [Create your first geofence](artwork-geojson)  

* [Restrict access to a gallery](artwork-config)  

* [Publish your work to IPFS](artwork-publish)  

## Part 2: gallery
* [Setup and publish your second project: gallery](gallery-setup)  

