{% include "unified-css.html" %}
{% include "current-project-banner.html" %}
[Back to the start of the tutorial](<quickstart#Let's get started>)
<br>
## Well done creating a new project!  
### You can already take a pic <a href="../preview/{{data.path.draft_dist_path}}/index.html" >at your project's content</a>

# Workflow: from project creation to publish
Helimura run through multiple steps to prepare the content of your work for publication to IPFS.

<img src="../../static/workflow.png" width="256"/>  
## 1. Creation
You ran this step in the previous section of this tutorial
## 2. Draft
Is the crucial step where you, the creator, generate, customize and test your content
## 3. Build
Add automatically generated files to your work, depending on values present in your configuration file (**{{data.path.config_name}}**),
and template files.  
As a common user, you shouldn't need to run this step by yourself.  
## 4. Release
During this step, your content from **{{data.path.draft_dist_path}}** is copied into an external folder, **{{data.path.release_public_path}}**.  
This external folder is identical to the one published to IPFS in the future.  
For common users, this step is run alongside publish by default, thus you shouldn't need to run it by yourself.   
## 5. Publish
Last step. During this step, the folders generated during release will be pushed to IPFS as is.  
<br>
# Okay! Off with the theory!!
### On next page, you will [create your first geofence](artwork-geojson)

<br>
---
### Annexe for advanced users
<details markdown="1">
<summary>Technical information</summary>

#### The path to your draft directory is as follows:
**{{data.path.draft_path}}**  
You can freely change the content of this directory. During the last step (publish), the files under **{{data.path.draft_dist_path}}** will be the files your visitors will see.  
This is the directory you could preview in the top link of this page.  
#### If you look under **{{ data.path.build_source_dirname }}**, you will see one or more text files.  
These files are called *Jinja templates*. During build, Helimura will apply values of parameters from your project's configuration file,
to these Jinja templates, and paste the output into **{{data.path.build_path}}**, alongside your own files.  
As you will see when we add *access control* to your project later in this tutorial, such a template system can be quite useful.  
Visit [Jinja official documentation](https://jinja.palletsprojects.com/en/stable/) For more informations on Jinja templates.   

#### Each step can be invoked in command line:
`helimura init webpage --name my-artwork [path to your project]`
`helimura build [path to your project]`  
`helimura release [path to your project]`
`helimura publish [path to your project]`
</details>
