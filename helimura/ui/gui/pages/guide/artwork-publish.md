{% include "unified-css.html" %}
{% include "current-project-banner.html" %}
[Back to the start of the tutorial](<quickstart#Let's get started>)

# Your project is soon to be published to IPFS
This process is straightforward:
### Navigate to <a href="../setup/publish" >setup/publish</a>
and follow the instructions under section <em>Publish your project</em>  
If everything goes as expected, follow the link to your IPFS node on the page, and check your online content.  

# Your project is published
Well done! A lot of new information may have been dumped unto you so far.  
IPFS is a new concept with many unfamiliar notions such as *content addressing*. Nevertheless, this decentralized network
holds a great potential for endless applications.  
### How to see your online content on your local IPFS gateway
{% include "cid-retrieve-tip.html" %}
# Congratulations on your first online content!
You know almost everything about Helimura.  
But this is with the second part of this tutorial that we will see its full potential.  
Grab a coffee, give yourself a pat on the back for following through,
## and come back for the final part: [Setup a gallery](gallery-setup)
