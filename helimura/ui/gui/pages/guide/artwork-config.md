{% include "unified-css.html" %}
{% include "current-project-banner.html" %}
[Back to the start of the tutorial](<quickstart#Let's get started>)
# Restrict access to the future gallery
Remember that in this tutorial, you, the creator, publish your work to IPFS, and allow a virtual gallery to redirect its visitors to your online project.  
On the other hand, your content will be unavailable for visitors coming from somewhere else.       
There are two requirements to fulfill this scenario:  

* The creation of a whitelist of authorized URL, including the URL of the gallery  
* To make sure that the project is available only if it is loaded from within an <a href="https://www.scientecheasy.com/2024/04/iframe-in-html.html/">IFrame</a>  

![](../static/iframe.svg)  
<br>
<br>
# But first,
### now is the time to verify whether your local IPFS node is up and running
#### Your current IPFS node address is <a href="{{ data.ipfs_node }}/webui" target="_blank">{{data.ipfs_node}}</a>
#### Your current IPFS gateway address is <a href="{{ data.ipfs_gateway }}/ipfs" target="_blank">{{data.ipfs_gateway}}</a>
For now, make sure that the first address goes to your IPFS node's dashboard,  
while the second URL show a page like the one in this screenshot:  
<br>
![](../static/2025-02-09_16-20.png)  
<br>
<details markdown="1">
<summary>Troubleshoot</summary>
#### 1. URL to IPFS node doesn't work
Open the app you use to start your IPFS node, and look for the correct address in the parameters  
If you don't have a running IPFS node, install it <a href="https://docs.ipfs.tech/install" target="_blank">https://docs.ipfs.tech/install</a>
#### 2. URL to the IPFS gateway doesn't work
* On the IPFS node dashboard page, find the *Advanced* collapsible menu, and deploy it, as shows the image below. The URL of your node gateway should be visible in this menu  
* Take note of the URL of your node gateway  
![](../static/ipfs_node.png)  
* If the adress looks like this: *http://0.0.0.0:8080*, note *http://127.0.0.1:8080* instead  
</details>
<br>
## A first look at the user configuration file
On the user configuration file page:

* You can freely modify the parameters under the category "Basic information"  
* Under section **"IPFS settings"** verify that the URL of your IPFS node and the URL of your IPFS gateway are correct. Fix them accordingly  
<br>
### Visit the <a href="../setup/info" >project configuration page</a> and see its content  
# Set access restriction on the user configuration file page
### Under "Template variables", see the section named *access*  

* First things first, activate the access restriction mode, by setting the option *locked* to 'true'  
* Under *whitelist*, *release*, the URL to your IPFS gateway is aready set by default.  
 *But what if we want to test this project **before** it is published to IPFS?*  
 In this case, we need to add the address of this tutorial, to the whitelist.  

Find the section *whitelist->draft*, and add the domain of this tutorial: <p id="this-page-domain"></p>
The result should look like this  
![](../static/2025-02-08_21-27.png)  

<br>
#### Explanation
The section is splitted into two parts, draft and release. URL under *draft* will be used for whitelisting access to your content,
while your project is visited on your computer.  
On the other hand, when your content will be remotely accessible on IPFS, the URL under *release* will be used for the whitelist.  

## Click on *Save modification* and test your content
Remember the second requirement to access your content: it can only be accessed from an IFrame belonging to a domain in your whitelist.  
We prepared a special preview page for you to test your work from an iframe, and without one. To do so, follow the two links below  
### If the whitelist is properly set, the first link below grants you access to your content, when you push start. But the second link denies you access:

* <a href="../preview/frame" >Access from iframe</a>
* <a href="../preview/{{data.path.draft_dist_path}}/index.html" >Access without iframe </a>  

<br>
## Phew, that was a big one
Let's go to the (disclaimer: somewhat) fun part of this tutorial:  
### Next step, [Publish your work to IPFS](artwork-publish)

---
### Annexe for advanced users
<details markdown="1">
<summary>Details</summary>
### Jinja templates
Helimura works with [Jinja template system](https://jinja.palletsprojects.com/en/stable/) to fill placeholders with values picked from your configuration file.   
For example, the following line in a jinja file  

{% raw %}
`var authorName = {{ data.author }}";`  

becomes  

`var authorName = "John Doe";`  
{% endraw %}

In your project by default, there is a folder named **{{data.path.build_source_dirname}}**, that mirrors the file structure of your draft folder.  
During the build step, these files are "rendered", and the output is inserted into your draft folder, with respect to the file structure from the template directory.  
These "rendered" files use the variables set in **{{data.path.config_name}}** to render their content.  
The usage of the whitelist we prepared in this tutorial can be checked in the Jinja template *content/template/access/dist/index._jinja_.js*  
And you can see the result in the rendered file *content/draft/access/dist/index._jinja_.js*
### IPFS gateway
More details about IPFS gateway <a href="https://docs.ipfs.tech/concepts/ipfs-gateway/" target="_blank">here</a>
</details>
<script>
var p = document.getElementById("this-page-domain");
p.innerText = window.location.origin;
</script>
