{% include "unified-css.html" %}
{% include "current-project-banner.html" %}
[Back to the start of the tutorial](<quickstart#Let's get started>)
# Create your first geofence
A geofence is a piece of data representing the exclusive area in which your work will be available to internet visitors.  
Your project can have as many geofences as you want.   
The piece of data is contained in what we call a "Geojson file".  
Next stop, we will generate a Geojson file and import it to your project.  
<br>
## Part 1: choose where to display your project in the real world
For convenience, you will use an external site, named *geojson.io*  
### On the interactive map, create a polygon for each geofence you want
<img src="../../static/2025-02-03_17-28.png" width="400"/>  
#### In the screenshot below, two geofences would be generated, for the area of France, and Spain
<img src="../../static/2025-02-03_17-30.png" width="350"/>  
### Save a Geojson file when you are done  
<img src="../../static/2025-02-03_17-30_1.png" width="350"/>  
### Head to the interative map at <a href="https://geojson.io" target="_blank">https://geojson.io</a> to choose your own geofences.   
<br>
## Part 2: Import the geojson file you saved, to the project  

Next, you will go to page *import Geojson files*.  
There, do as follow:    

* navigate to section **Import a GEOJSON file**  
* choose **Option 1** to upload the file you just created  
* don't forget to hit the button "Import GEOJSON"  
### Go to <a href="../setup/geojson" >import Geojson files</a>  

<br>

### If you successfully uploaded the Geojson file, time to proceed to next section
* You can preview your new geofence <a href="../browse/content/preview/geojson/index.html" target="_blank">on an interactive map</a>    
<br>
## Next stop, we will [restrict access to a gallery](artwork-config)

<br>
---
### Annexe for advanced users
<details markdown="1">
<summary>Technical information</summary>
You can use this command line from your terminal as well

```helimura geojson import --fromfile [path to your geojson file] [path to your project directory]```  
</details>
