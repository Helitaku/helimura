
{% include "unified-css.html" %}
{% include "current-project-banner.html" %}
[Back to the start of the tutorial](<quickstart#Let's get started>)


<br>
# Create a gallery

Reminder: [Project workflow: from creation to publish](artwork-files)

<br>
### You will now create and publish a gallery.  
Your visitors will first access the online gallery, then the gallery opens an <a href="https://www.scientecheasy.com/2024/04/iframe-in-html.html/">IFrame</a> pointing to an artwork close to the visitor's location.  

* To know what location redirects to one artwork, a gallery first imports the geofences of the artwork.  
* Therefore, it downloads said geofences, and store the url of the artwork they belong to.  
* When a visitor connects to the gallery page, the gallery goes through all the geofences it imported, and checks if there
is a match.

<br>
Let's start with creating the gallery.  
If you finished the first part of this tutorial, the following should be straightforward.  
<br>
### Go to <a href="../setup/new" >the project creator page</a> but this time, select the template **gallery**  

If you preview your content now, there is nothing much to see. The interfaces displays a map, and hangs there.  
This is because this gallery is not linked to any other published project.
<details markdown="1">
<summary>Technical information</summary>
Equivalent of command line  

```helimura init gallery [path to your future project directory]```  
</details>

<br>
<br>
# Import the geofences of your published artwork
Do you remember how to retrieve the CID of your project once it is online?
<details>
<summary>If not, click to see a reminder</summary>
{% include "cid-retrieve-tip.html" %}
</details>
This time, we don't want the CID of the artwork static site(**{{data.path.release_public_name}}**), but the CID of the geofences,
published alongside it.

* Do the exact same procedure as the one described in the reminder, but right click on the directory containing the suffix
**{{data.path.release_geofence_name}}**, to copy its CID  
* Determine the url to the geofences, as you did for {{data.path.release_public_name}}, because we will use it in the next section  

<br>
## Setup the link
### Go to the setup page **<a href="../setup/link">Create a reference to another project</a>** 
Under the *Fetch* section, fill the text field with the URL you just stored, and click on *Fetch reference*.  
This will fetch essential information on the published geofences, but not download them yet.    

If everything goes as expected, the text field under *redirect* filled itself with a URL.  
This URL is the remote static site (your published artwork) these geofences are related to.  
You have a chance to correct it, before adding the geofences to your gallery.  

Click on **Proceed** to finish the import.  

## Your first link!
Now that your first external project is linked to the gallery, you may want to try out the result:    
### Preview  <a href="../preview/{{data.path.draft_dist_path}}/index.html" >your first gallery project</a>
### You will be greeted by a **Access not granted** message from the online artwork. This is expected  
The cause is you visiting the artwork from this tutorial's domain: **this URL is not in the artwork's whitelist**  
#### Good news is that the address of your gallery, once it is published on your IPFS node, is amongst the addresses whitelisted for the artwork.  
Which brings us to the last section of this tutorial.  

<br>
# Almost done for real this time: Publish the gallery
This step should be straightforward as well:

Navigate to <a href="../setup/publish" >setup/publish</a>
and follow the instructions under section <em>Publish your project</em>  
If everything goes as expected, follow the link to your IPFS node on the page, and check your online content.  

## Once again, determine the URL to your gallery, and test out your fresh new content
<details>
<summary>Click here if you don't remember how to determine the URL</summary>
{% include "cid-retrieve-tip.html" %}
</details>
<br>
# Thank you!
Thank you for sticking to this tutorial until its very end.  
<img src="../../static/cat.jpg"/>  
If you have any question, contact me @ [sanliberg@helitaku.com](mailto: sanliberg@helitaku.com)
