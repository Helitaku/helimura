import argparse
import os
import sys
import logging
import subprocess
import helimura.parser as mainparser
from helimura.core import project, runtime

parse_arguments_defaults = {
    # "id": {
    #     "requires_project": True,
    #     "help": "Print the automatically generated unique ID of this project.",
    #     "parameters": {
    #         "set": {
    #             "option_strings": "--set",
    #             "default": "",
    #             "help": "For advanced users. The unique ID of the project can be manually changed with this flag.",
    #         },
    #         "generate": {
    #             "option_strings": "--generate",
    #             "action": "store_true",
    #             "help": "For advanced users. (Re)generate a unique ID for this project.",
    #         },
    #     },
    # },
    "demo": {
        "requires_project": False,
        "help": "executes demo.sh as a subprocess",
        "parameters": {},
    },
    "config": {
        "requires_project": True,
        "help": "Everything related to the handle of the configuration file of this project",
        "parameters": {
            "config_action": {"choices": ["show", "import", "export"]},
            "-f": {"default": "", "help": "Path to the YAML file to import/export."},
        },
    },
    "init": {
        "requires_project": False,
        "help": "",
        "parameters": {
            "template": {
                # It is important to keep the favorite template as the first value in this list, so that the GUI will show this first entry as default at project creation
                "choices": ["webpage", "gallery", "threejs", "empty"],
                "help": "Template available for project creation",
            },
            "name": {
                "option_strings": "--name",
                "help": "Name for the new project.",
                "default": "",
            },
            # With "requires_project" to False, we make this parameter optional. If not set and if --name is set, a default path will be used instead
            "project_directory": {
                "nargs": "?",
                "default": "",
            },
        },
    },
    "build": {
        "requires_project": True,
        "help": "Build project's content with jinja templates from the template folder, values from the config file, and files from the draft folder.",
        "parameters": {},
    },
    "access": {
        "requires_project": True,
        "help": "Used to whitelist the url authorized to load the content in a web browser.",
        "parameters": {
            "access_action": {"choices": ["list", "add", "remove"]},
            "mode": {"option_strings": "--mode", "default": "release"},
            "parameters": {
                "nargs": "?",
                "help": "index, if used with argument 'remove', to specify which entry to delete. URL if used with argument 'add'.",
            },
        },
    },
    "geojson": {
        "requires_project": True,
        "help": "",
        "parameters": {
            "geojson_action": {"choices": ["add", "list", "show", "remove"]},
            "fromfile": {
                "option_strings": "--fromfile",
                "type": str,
                "default": "",
                "help": "Used with argument 'add'. Path to the GEOJSON file to import.",
            },
            "fromdir": {
                "option_strings": "--fromdir",
                "default": "",
                "type": str,
                "help": "Used with argument 'add'. Batch import GEOJSON files inside the specified directory.",
            },
            "fromtxt": {
                "option_strings": "--fromtxt",
                "type": str,
                "default": "",
                "help": "Used with argument 'add'. Import GEOJSON from text.",
            },
            "fromstdin": {
                "type": argparse.FileType("r"),
                "default": sys.stdin,
                "option_strings": "--fromstdin",
            },
            "name": {
                "option_strings": "--name",
                "type": str,
                "default": "",
                "help": "Used with arguments 'remove' and 'show', and with argument 'add', if you use the flag '--fromstdin' \
or '--fromtxt'. Name of the imported GEOJSON file. Use 'geojson list' to check the list of imported GEOJSON files.",
            },
        },
    },
    "geofence": {
        "requires_project": True,
        "help": "",
        "parameters": {
            "geofence_action": {
                "choices": [
                    "list",
                    "content",
                    "download",
                    "redirect",
                    "reset",
                ],
                "help": "\
list -> Show various information about the downloaded geofences, such as the url where a visitor rhould be redirected when within \
a geofence.\n\
download ->  Download another project's set of geofences, and merge the project's quadtree with the quadtrees of the other \
downloaded projects.\neg. 'helimura download https://<gateway>/ipfs/<cid-of-published-geofences> /my/project/path'\n\
redirect ->  For a set of geofences belonging to a previously downloaded project, set the url to where a visitor should be \
redirected once within one of these geofences. If not sure, check info with helimura geofence list\n\
eg. 'helimura redirect https://url-to-access.content cidOfPublishedGeofences /my/project/path'\n\
reset ->  Removes all references to other projects.",
            },
            "gateway": {
                "option_strings": "--gateway",
                "help": "usage: 'helimura download --gateway https://<new-gateway-for-redirection> https://<gateway>/ipfs/<cid-of-published-geofences> /my/project/path'",
                "default": "",
            },
            "extra": {
                "help": "Not to be used directly",
                "nargs": "*",
            },
        },
    },
    "fetch": {
        "requires_project": False,
        "help": "Provided its URL, prints the metadata file of a project published on IPFS.",
        "parameters": {
            "url": {
                "help": "URL used to fetch metadata. It must end with the name of the metadata file."
            },
        },
    },
    "pull": {
        "requires_project": False,
        "help": "Pull the project source code from IPFS, to the draft directory",
        "parameters": {
            "node": {
                "option_strings": "--node",
                "help": "Address of an IPFS node to use instead of the value set in $IPFS_NODE or in this project's configuration file",
                "default": "",
            },
            "cid": {"help": "CID of the project to retrieve"},
        },
    },
    "prepublish": {
        "requires_project": True,
        "help": "Compile the project into a 'release' version, ready to be pushed to IPFS. Cf. the command 'push'",
        "parameters": {},
    },
    "publish": {
        "requires_project": True,
        "help": "Push the 'release' version of this project to IPFS. Cf. the command 'prerelease'",
        "parameters": {
            "name": {
                "option_strings": "--name",
                "help": "Overrides the name of this project, for directory name on IPFS.",
                "type": str,
                "default": "",
            },
            "comment": {
                "option_strings": "--comment",
                "help": "Store your comment in the metadata file pushed with the project. \
May be useful to remind you why you pushed this version, or when sharing it with collaborators.",
                "type": str,
                "default": "",
            },
            "last": {
                "option_strings": "--last",
                "help": "Print the CID of the last version, for each folder published for this project",
                "default": None,
            },
            "ipfs_node": {
                "type": str,
                "default": None,
                "option_strings": "--ipfs_node",
            },
            "skip_prepublish": {
                "action": "store_true",
                "option_strings": "--skip_prepublish",
                "help": "Publish also does a prepublish by default. This can be bypassed with this option, if you prefer to do the prepublish manually, before publishing",
            },
        },
    },
}


def parse_args():
    command, args = mainparser.parse_args(parse_arguments_defaults)
    # runtime.initialize()
    if args:
        parse_command(command, args)
    return args


def parse_command(command, _args):

    if type(_args) == dict:
        args = _args

    else:
        args = _args.__dict__

    if command == "geojson":
        if (
            args["geojson_action"] == "add"
            and (args["fromtxt"] or args["fromstdin"])
            and not args["name"]
        ):
            raise ValueError(
                "A name must be set with argument 'add' and this optional argument. Use '--name' "
            )
        elif args["geojson_action"] == "remove" and not args["name"]:
            raise ValueError(
                "You must specify a name with this command. Example: helimura geojson remove --name 'tokyo.json' ./path/to/myproject.\n\
    Check the list of names with 'helimura geojson list ./path/to/myproject"
            )

    project_directory = args.get("project_directory") or ""
    if parse_arguments_defaults[command]["requires_project"]:
        if not os.path.exists(project_directory):
            project_directory = os.path.join(
                runtime.program_project_directory, project_directory
            )
            if not os.path.exists(project_directory):
                raise OSError("Project doesn't exist: " + project_directory)

    # if command == "id":
    #     content = module.as_project(args["project_directory"])
    #     if args["set"]:
    #         c = content.metadata.load()
    #         c.set("id", args["set"])
    #         c.dump()
    #     elif args["generate"]:
    #         content.generate_id()
    #     else:
    #         print(content.metadata.load().get("id"))
    if command == "demo":
        subprocess.run(os.path.join(runtime.script_directory, "demo.sh"))
    elif command == "init":
        name = args["name"]
        if project_directory:
            if not os.path.exists(project_directory):
                if not name:
                    name = project.FileManager.default_project_name
        else:
            if not args["name"]:
                raise ValueError(
                    "Neither a name, nor a project directory was specified. Either specify a name with flag --name, or append the path to a new directory, to this command."
                )
            name = args["name"]
        project_path = project_directory or os.path.join(
            runtime.program_project_directory, name
        )
        if os.path.exists(project_path):
            logging.warning("Project already exists: aborted.")
        else:
            proj = project.as_project(project_path)
            proj.init(template=args["template"], name=name)
    elif command == "build":
        project.as_content(project_directory).build()

    elif command == "prepublish":
        project.as_project(project_directory).prepublish()

    elif command == "publish":
        if not args.get("last") == None:
            if args["last"] == "all":
                project.as_project(project_directory).print_last_publish_info()
            else:
                project.as_project(project_directory).print_last_publish_info(
                    args["last"]
                )
        else:
            project.as_project(project_directory).publish(
                published_name=args.get("name"),
                comment=args.get("comment"),
                ipfs_node=args.get("ipfs_node"),
                skip_prepublish=args.get("skip_prepublish"),
            )

    elif command == "pull":
        project.as_project(None).pull(
            args["cid"], project_directory, ipfs_node=args["node"]
        )

    elif command == "fetch":
        path = args["url"]
        project.Metadata.fetch(path)

    elif command == "config":
        proj = project.as_content(project_directory)
        user_config = proj.config

        if args["config_action"] == "show":
            user_config.read()
            proj = project.as_project(project_directory)
            project_config = proj.config
            project_config.read()
        elif args["config_action"] in ["import", "export"] and not arg["f"]:
            raise ValueError(
                "You must use this command with '-f' to set the full path of the file you want to import/export"
            )
        if args["config_action"] == "import":
            user_config.overwrite_from_file(arg["f"])
            project.as_content(project_directory).build()

        elif args["config_action"] == "export":
            user_config.export(arg["f"])

    elif command == "access":
        proj = project.as_content(project_directory)
        project_config = proj.config
        project_config.load()
        if args["access_action"] == "list":
            whitelist = project_config.get("access")["whitelist"]
            # prints:
            # draft:
            # 0 http://1st-url.com
            # 1 http://2nd-url.com
            # release:
            # 0 http://3rd-url.com
            for access in ["draft", "release"]:
                print(access + ":")
                print(
                    "\n".join(
                        [
                            f"{x} {y}"
                            for x, y in zip(
                                range(len(whitelist.get(access) or [])),
                                whitelist.get(access) or [],
                            )
                        ]
                    )
                )
            #
        else:
            authorized_access = project_config.get("access")
            if args["access_action"] == "add":
                whitelist = authorized_access["whitelist"][args["mode"]] or []
                authorized_access["whitelist"][args["mode"]] = whitelist + [
                    args["parameters"]
                ]
                authorized_access["locked"] = True
            elif args["access_action"] == "reset":
                authorized_access["whitelist"][args["mode"]] = []
            project_config.set("access", authorized_access)
            project_config.dump()
            proj.build()

    elif command == "geojson":
        geo = project.as_geojson(project_directory)
        if args["geojson_action"] == "add":
            for key in args:
                if key == "fromfile":
                    geo.from_file(args["fromfile"])
                    break
                elif key == "fromdir":
                    for fi in geo.yield_geojson_files(args["fromdir"]):
                        geo.from_file(fi)
                    break
                elif key == "fromstdin":
                    geo.from_txt(
                        args["name"]
                        + (".geojson" if not args["name"].endswith(".geojson") else ""),
                        args["fromstdin"].read(),
                    )
                    args["fromstdin"].close()
                    break
                elif key == "fromtxt":
                    geo.from_txt(
                        args["name"]
                        + (".geojson" if not args["name"].endswith(".geojson") else ""),
                        args["fromtxt"],
                    )
                    break

        elif args["geojson_action"] == "list":
            print("\n".join(geo.imported))
        elif args["geojson_action"] == "remove":
            geo.remove(args["name"])
        elif args["geojson_action"] == "show":
            geo.show(args["name"])

    elif command == "geofence":
        if args["geofence_action"] == "list":
            c = project.as_geofence(project_directory).config
            if os.path.exists(c.path):
                c.read()
        elif args["geofence_action"] == "redirect":
            path, cid = args["extra"]
            project.as_geofence(project_directory).add_redirect(cid, path)
        elif args["geofence_action"] == "content":
            cid = args["extra"][0]
            print(
                (
                    project.as_geofence(project_directory).config.load().get(cid) or {}
                ).get("content")
            )
        elif args["geofence_action"] == "download":
            extra = args["extra"]
            path = extra[0]
            gateway = None
            project.as_geofence(project_directory).download(
                path, gateway=args.get("gateway")
            )
        elif args["geofence_action"] == "reset":
            project.as_geofence(project_directory).reset()
