import argparse
import os
import sys


def get_command():
    parser = argparse.ArgumentParser(add_help=False)
    _, unknown = argparse.ArgumentParser(add_help=False).parse_known_args()
    if unknown:
        return unknown[0]
    return None


def parse_args(defaults):
    command = get_command()
    commands_available = defaults.keys()
    commands_help = "\n".join([f"{k}: {defaults[k]['help']}" for k in defaults])

    if command not in commands_available or command in ["-h", "--help"]:
        parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
        parser.add_argument("command", choices=commands_available, help=commands_help)
        parser.print_help()
        return command, None

    parser = argparse.ArgumentParser(formatter_class=argparse.RawTextHelpFormatter)
    parser.add_argument("command", choices=[command], help=defaults[command]["help"])

    if not command in commands_available:
        return command, None
    argument_dict = defaults[command]
    requires_project = argument_dict["requires_project"]
    parameters_dict = argument_dict["parameters"]
    for name, flags in parameters_dict.items():
        arg_name = flags.get("option_strings") or name
        parser.add_argument(arg_name, **flags)
    if requires_project == True:
        parser.add_argument(
            "project_directory", help="Path to the project we want to create or modify"
        )

    args = parser.parse_args()
    return command, args
