const duration = 12000;
let ended = false;

async function drawScene(){
    const THREE = await import('../node_modules/three/build/three.module.js');
    const { FlyControls } = await import('../node_modules/three/examples/jsm/controls/FlyControls.js');
    const { ImprovedNoise } = await import('../node_modules/three//examples/jsm/math/ImprovedNoise.js');

    const white = 0xffffff;
    const black = 0x000000;
    let camera, controls, scene, renderer;
    let landscapeMesh, texture;
    let billboardMesh1, billboardGroup;
    const worldWidth = 256, worldDepth = 256;
    const billboardSizeX = billboardSizeY = 51;
    init();
    let startTime = Date.now();
    animate();

    function init() {
        billboardGroup = new THREE.Group();
        // camera = new THREE.PerspectiveCamera( 60, window.innerWidth / window.innerHeight, 1, 10000 );
				camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 10000 );
				// camera.position.z = 500;
        scene = new THREE.Scene();
        var backgroundColor = '#26f3e2';//0x00000;//0xefd1b5;
        scene.background = new THREE.Color(backgroundColor );
        scene.fog = new THREE.FogExp2(backgroundColor, 0.001 );

        const data = generateHeight( worldWidth, worldDepth );

        // camera.position.set( 100, 800, - 800 );
        // camera.lookAt( - 100, 810, - 800 );

        const landscapeGeom = new THREE.PlaneGeometry( 7500, 7500, worldWidth - 1, worldDepth - 1 );
        landscapeGeom.rotateX( Math.PI / 1.3 );

        const vertices = landscapeGeom.attributes.position.array;

        for ( let i = 0, j = 0, l = vertices.length; i < l; i ++, j += 3 ) {

            vertices[ j + 1 ] = data[ i ] * 10;

        }

        const light = new THREE.DirectionalLight( white, 7 );
        light.position.set( 0.5, 1, 1 ).normalize();
        scene.add( light );

        landscapeMesh = new THREE.Mesh( landscapeGeom, new THREE.MeshBasicMaterial( { wireframe: true } ) );
        landscapeMesh.position.y -= 600;
        // mesh.position.z -= 2000;
        scene.add( landscapeMesh );

        const billboardTex1 = new THREE.TextureLoader().load( "media/sample.png" );
        const billboardMat1 = new THREE.MeshBasicMaterial( { map: billboardTex1, depthTest: false, transparent: true } );
        let billboardGeom1 = new THREE.PlaneGeometry( billboardSizeX, billboardSizeY, 4, 4);
        billboardMesh1 = new THREE.Mesh( billboardGeom1, billboardMat1);
        billboardGroup.add(billboardMesh1);

        const billboardTex2 = new THREE.TextureLoader().load( "media/logo.png" );
        const billboardMat2 = new THREE.MeshBasicMaterial( { map: billboardTex2, depthTest: false, transparent: true } );
        let billboardGeom2 = new THREE.PlaneGeometry( billboardSizeX, billboardSizeY, 4, 4);
        billboardMesh2 = new THREE.Mesh( billboardGeom2, billboardMat2);
        billboardMesh2.rotateY(Math.PI);
        billboardMesh2.position.z -= 7;
        billboardGroup.add(billboardMesh2);

        billboardGroup.position.z = 400;
        scene.add( billboardGroup );
        renderer = new THREE.WebGLRenderer();
        renderer.setPixelRatio( window.devicePixelRatio );
        renderer.setSize( window.innerWidth, window.innerHeight );
        document.body.replaceChildren( renderer.domElement );
        
        controls = new FlyControls( camera, renderer.domElement );
        controls.movementSpeed = 0;
        controls.domElement = renderer.domElement;
        controls.rollSpeed = Math.PI / 240;
        controls.autoForward = false;
        controls.dragToLook = false;

        window.addEventListener( 'resize', onWindowResize );

    }

    function onWindowResize() {

        camera.aspect = window.innerWidth / window.innerHeight;
        camera.updateProjectionMatrix();

        renderer.setSize( window.innerWidth, window.innerHeight );

        controls.handleResize();

    }

    function generateHeight( width, height ) {
        let seed = Math.PI / 4;
        window.Math.random = function () {
            const x = Math.sin( seed ++ ) * 10000;
            return x - Math.floor( x );
        };
        const size = width * height, data = new Uint8Array( size );
        const perlin = new ImprovedNoise(), z = Math.random() * 100;
        let quality = 1;

        for ( let j = 0; j < 4; j ++ ) {
            for ( let i = 0; i < size; i ++ ) {
                const x = i % width, y = ~ ~ ( i / width );
                data[ i ] += Math.abs( perlin.noise( x / quality, y / quality, z ) * quality * 1.75 );
            }
            quality *= 5;
        }
        return data;

    }

    //
    function animate() {
        requestAnimationFrame( animate );
        const remainingTime = duration - ( Date.now()-startTime );
        const seconds = parseInt(remainingTime/1000);
        if(remainingTime>0){
            camera.position.z = 570 + (130 * (remainingTime/duration));
            billboardGroup.rotateY(0.005*(remainingTime/duration));
            render();
        } else if (!ended) {
            ended = true;
            window.location.href = "support-us/index.html";
        } 
    }

    function render() {
        // controls.movementSpeed = 10;
        controls.update( 0.1 );
        renderer.render( scene, camera );
    }

}

module.exports = {
  run: function () {
    audio = document.getElementById( 'audio' );
    audio.play();
    drawScene();
  }
};

