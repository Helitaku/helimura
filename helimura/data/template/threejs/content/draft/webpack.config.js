const path = require('path');
const webpack = require('webpack');
module.exports = {
    mode: "production",
    entry: "./src/index.js",
    plugins: [
    	new webpack.optimize.LimitChunkCountPlugin({
      		maxChunks: 1,
    	}),
  	],
    output: {
        filename: 'app.js',
    	libraryTarget: 'var',
    	library: 'EntryPoint',
        path: path.resolve(__dirname, 'dist'),
    },
    devServer: {
        static: './dist',
    }
};
