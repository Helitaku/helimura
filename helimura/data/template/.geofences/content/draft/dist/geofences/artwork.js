
const quadtreeURL = "./data";
let position;

function insidePolygon(point, vs) {
  // Found on https://stackoverflow.com/questions/22521982/check-if-point-is-inside-a-polygon
  // ray-casting algorithm based on
  // https://wrf.ecse.rpi.edu/Research/Short_Notes/pnpoly.html

  var x = point.x, y = point.y;

  var inside = false;
  for (var i = 0, j = vs.length - 1; i < vs.length; j = i++) {
      var xi = vs[i][0], yi = vs[i][1];
      var xj = vs[j][0], yj = vs[j][1];

      var intersect = ((yi > y) != (yj > y))
          && (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
      if (intersect) inside = !inside;
  }

  return inside;
};
class Point {
  constructor(geolocation) {
      this.x = geolocation.coords.longitude;
      this.y = geolocation.coords.latitude;
      this.radius = geolocation.coords.accuracy;
  }
  isInside(bbox) {
      return this.x > bbox[0] && this.x < bbox[2] &&
          this.y > bbox[1] && this.y < bbox[3];
  }
  isOutside(vMin, vMax) {
      return this.x > vMax.x || this.y > vMax.y ||
          this.x < vMin.x || this.y < vMin.y;
  }
  async distanceTo(lat2, lon2) {
      const lat1 = this.x;
      const lon1 = this.y;
      var p = 0.017453292519943295;    // Math.PI / 180
      var c = Math.cos;
      var a = 0.5 - c((lat2 - lat1) * p) / 2 +
          c(lat1 * p) * c(lat2 * p) *
          (1 - c((lon2 - lon1) * p)) / 2;
      return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
  }
}
function insideBoundary(position, children) {
  for (let i in children) {
      var bbi = children[i];
      if (position.isInside(bbi.bbox)) {
          return bbi;
      }
  }
  return null;
}

async function checkArtworksInArea(position, artworks, radius) {
  if (!artworks.length) {
      return [];
  }
  else {
      let matches = [];
      for (let a in artworks) {
          let artwork = artworks[a];
          if (position.isInside(artwork.bbox)) {
              matches = matches.concat(artwork);
          }
      }
      return matches;
  }
}

async function recursiveSearch(position, metadataURL, radius = 5000) {
  // This recursive function parses a quadtree and finds the smallest bbox with artworks inside, and fitting the user's location
  const metadata = await fetch(metadataURL).then(response => response.json());
  const bbox = metadata.bbox;
  const children = metadata.children;
  let artworksWithinRange = await checkArtworksInArea(position, metadata.features, radius);
  let artworksInArea = metadata.features;
  let within = await insideBoundary(position, children);
  if (within) {
      if (within.filename) {
          var [awr, aia, bb] = await recursiveSearch(position, `${quadtreeURL}/${within.filename}`, radius);
          artworksWithinRange = artworksWithinRange.concat(awr);
          artworksInArea = artworksInArea.concat(aia);
          return [artworksWithinRange, artworksInArea, bb];
      }
  } else {
      //We gather artworks outside range of the user's position but still close
      for (let i in children) {
          bi = children[i];
          var m = await fetch(`${quadtreeURL}/${bi.filename}`).then(response => response.json());
          artworksInArea = artworksInArea.concat(m.features);
      }
  }
  // Should be called once, when the smaller bbox fitting the user's location is reached
  return [artworksWithinRange, artworksInArea, bbox];
}

function formatLink(cid) {
  if (!cid) {
      return null;
  }

  var ipfsGateway = window.location.host;
  let _, ipfs, formattedLink, __ = "";
  var thisUrlMatch = window.location.host.match(/([a-zA-Z0-9\-]+)\.(ip.s)\.([a-zA-Z0-9\.\-\:]+)$/);
  if (thisUrlMatch) {
      [_, __, ipfs, ipfsGateway] = thisUrlMatch;
  } else {
      thisUrlMatch = window.location.pathname.match(/^\/(ip.s)\/([a-zA-Z0-9]+)\/?/);
      if (thisUrlMatch) {
          [_, ipfs, __] = thisUrlMatch;
      }
  }
  if (ipfs) {
      formattedLink = `${window.location.protocol}//${cid}.${ipfs}.${ipfsGateway}`;
  } else {
      formattedLink = `${window.location.protocol}//${defaultIPFSGateway}/ipfs/${cid}`;
  }
  return new URL(formattedLink);
}
async function findNearbyArtworks(currentPosition) {
  position = currentPosition;
  let [artworksWithinRange, artworksInArea, bbox] = await recursiveSearch(position, `${quadtreeURL}/0.json`);
  return [artworksWithinRange, artworksInArea]
}

async function findArtwork(artworksWithinRange) {
  let artwork = null;
  for (let i = artworksWithinRange.length - 1; i >= 0; i--) {
      // Theorically, looping from smaller to bigger artworks
      let a = artworksWithinRange[i];
      for (let c in a.geometry.coordinates) {
          var polygon = a.geometry.coordinates[c];
          if (insidePolygon(position, polygon)) {
              artwork = a;
              break;
          }
      }
  }
  return artwork; 
}
