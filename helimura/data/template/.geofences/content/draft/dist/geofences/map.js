let map, circle;
let featureGroup = [];

function initMap(currentMap, position) {
    map = L.map('map').setView([position.y, position.x], 18);
    const tiles = L.tileLayer('https://tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
    }).addTo(map);

    circle = L.circle([position.y, position.x], {
        color: 'blue',
        fillColor: 'blue',
        fillOpacity: 0.5,
        radius: position.radius
    });
    circle.addTo(map);
    featureGroup = featureGroup.concat(circle);

    document.getElementById("map").style.display = "block";
    map.invalidateSize(true);
    circle.bindTooltip("<p>近くのアート 作品を探す。。。</p>Searching nearest art content...</b>", { "opacity": 0.85, "permanent": true }).openTooltip();
  
}

function displayfeatureGroup(delay) {
  //   setTimeout(() => {
  //     map.off();
  //     map.remove();
  // }, delay)
  var group = new L.featureGroup(featureGroup);
  map.fitBounds(group.getBounds());
}

function displayArtwork(artwork, delay=3000) {
  var polygon = L.polygon(
      artwork.geometry.coordinates[0].map((x) => [x[1], x[0]]),
      {
          color: "green"
      });
  polygon.addTo(map);
  circle.bindTooltip("<p>You found an artwork!</p>\
      <H2 style=\"text-align: center\">"+ artwork.name + "</H2>\
      <p style=\"text-align: center\">Loading...</p>",
      { "opacity": 0.85, "permanent": true })
      .openTooltip();
  featureGroup = featureGroup.concat(polygon);
  displayfeatureGroup();
}

function displayArtworksInArea(artworksInArea) {
  for (let i in artworksInArea) {
      var a = artworksInArea[i];
      var polygon = L.polygon(
          a.geometry.coordinates[0].map((x) => [x[1], x[0]]),
          {
              color: "pink"
          });
      polygon.bindTooltip(a.name);
      polygon.addTo(map);
      featureGroup = featureGroup.concat(polygon);
  }
  circle.bindTooltip("There are artworks nearby!", { "opacity": 0.85, "permanent": true }).openTooltip();
  displayfeatureGroup();

}
