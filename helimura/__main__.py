import sys
import helimura.core.runtime as runtime
import helimura.ui.tui.parser as tparser
import helimura.ui.gui.parser as gparser

if __name__ == "__main__":
    if not len(sys.argv) > 1:
        # Kinda dirty trick, but if the command line has no argument,
        # and if this is a binary file, launch the webui
        # We are doing it, so a user only needs to double-click on the executable
        if runtime.is_pyinstaller_bundle:
            sys.argv.append("setup")
    if not gparser.parse_args():
        tparser.parse_args()
