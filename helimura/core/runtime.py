import os
import sys
import shutil
from helimura.core.config import ProgramConfig

program_name = "helimura"

is_pyinstaller_bundle = getattr(sys, "frozen", False) and hasattr(sys, "_MEIPASS")
package_parent_directory = os.path.abspath(os.path.dirname(os.path.dirname(__file__)))
program_directory = os.path.abspath(
    os.path.dirname(
        sys.executable
        if is_pyinstaller_bundle
        else os.path.dirname(os.path.dirname(__file__))
    )
)

program_config_env_var = "HELIMURA_CONFIG_FILE"
config_path = os.getenv(program_config_env_var)
config = ProgramConfig(
    directory=(
        os.path.dirname(config_path)
        if config_path
        else (
            program_directory
            if is_pyinstaller_bundle
            else ProgramConfig.default_directory
        )
    ),
    name=(
        os.path.basename(config_path)
        if config_path
        else ProgramConfig.default_file_name
    ),
)

binary_name = program_name + (".exe" if not os.sys.platform == "linux" else "")
program_document_directory = (
    lambda: config.load().get("document_dir") or program_directory
)()
program_project_directory = (
    lambda: config.load().get("project_dir")
    or os.path.join(os.path.expanduser("~"), ".helimura", "projects")
)()
pages_path = os.path.join("ui", "gui", "pages")
pages_directory = os.path.join(
    program_document_directory,
    program_name if not is_pyinstaller_bundle else "",
    pages_path,
)
template_path = os.path.join("data", "template")

template_directory = os.path.join(
    program_document_directory,
    program_name if not is_pyinstaller_bundle else "",
    template_path,
)
script_directory = os.path.join(
    program_document_directory,
    "scripts",
    "linux" if os.sys.platform == "linux" else "windows",
)
executable_document_directory = (
    getattr(sys, "_MEIPASS") if is_pyinstaller_bundle else ""
)

program_document_list = [
    ("docs", "docs"),
    ("scripts", "scripts"),
    ("LICENCE", ""),
    ("README.md", ""),
    (os.path.join(program_name, pages_path), pages_path),
    (os.path.join(program_name, template_path), template_path),
    (
        os.path.join(
            program_name, os.path.basename(config.default_directory), config.name
        ),
        "",
    ),
]
