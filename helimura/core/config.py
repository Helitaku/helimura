import os
import json
import yaml
import pyaml
import jinja2
import logging
import requests
from string import Template

program_config_env_var = "HELIMURA_CONFIG_FILE"


class _Metadata(object):
    """
    Handler of the metadata file, for convenience.
    """

    @property
    def path(self):
        return os.path.join(self._directory, self._name)

    @property
    def name(self):
        return self._name

    @property
    def directory(self):
        return self._directory

    def __init__(self, directory=".", name=None):

        name = name if name is not None else self.default_file_name
        self._directory = directory
        self.rename(name)
        self._content = {}
        self._readonly = False

    def __repr__(self):
        with open(self.path) as f:
            return f.read()

    def __ior__(self, other):
        self.update(other)
        return self

    def __or__(self, other):
        if issubclass(other.__class__, _Metadata):
            return self._content | other._content
        else:
            return self._content | other

    def exists(self):
        return os.path.exists(self.path)

    def keys(self):
        return self._content.keys()

    def copy(self):
        return self._content.copy()

    def delete_key(self, key):
        del self._content[key]

    def get(self, k):
        return self._content.get(k)

    def set(self, k, v):
        self._content[k] = v

    def update(self, data):
        if issubclass(data.__class__, _Metadata):
            self._content |= data._content
        else:
            self._content |= data

    def load(self):
        """
        Opens a metadata file, stores the parent directory of the file as an attribute of this class,
        store the content of the file (a JSON object) as a dictionary, and return the dictionary.

        :param parent_directory: Parent directory of the metadata file
        :type parent_directory: str

        :return:  the metadata of the open file
        :rtype: dict
        """
        if not os.path.exists(self.path):
            raise OSError("metadata file %s not found" % self.path)

    def read(self):
        if not self._content:
            self.load()
        print(self)

    @staticmethod
    def fetch(url, filename="", quiet=False):
        """
        Reads a metadata file directly from a URL and return its content
        """
        name = filename or Metadata.default_file_name
        response = requests.get(f"{url}/{name}")
        if response.status_code == 200:
            self = _Metadata(name=name)
            self._content = response.json()
            if not quiet:
                print(json.dumps(self._content))
            return self
        else:
            txt = (
                response.text[:500]
                + "\n"
                + "Couldn't fetch metadata: status code %s" % response.status_code
            )
            raise requests.ConnectionError(txt)

    def expand(self, parameters=os.environ):
        self._content = self.expanded(parameters)

    def expanded(self, parameters=os.environ):
        e = jinja2.Environment()
        s = e.from_string(str(self._content))
        return eval(s.render(**parameters))

    def rename(self, new_name):
        self._name = new_name

    def dump(self, directory=None):
        if self._readonly:
            raise OSError("Cannot dump metadata into file, as it is set to 'readonly'")
        directory = directory or self._directory
        self._dump(os.path.join(directory, self._name))


class Metadata(_Metadata):
    default_file_name = "metadata.json"

    def _dump(self, target):
        with open(target, "w") as f:
            json.dump(self._content, f)

    def load(self):
        super().load()
        with open(self.path) as f:
            self._content = json.load(f)
        return self


class Config(_Metadata):
    default_file_name = "settings.yaml"

    def _dump(self, target):
        with open(target, "w") as f:
            yaml.safe_dump(self._content, f, sort_keys=False)

    def load(self):
        super().load()
        with open(self.path) as f:
            self._content = yaml.safe_load(f) or {}
        return self

    def overwrite_from_file(self, file):
        if self.__readonly:
            raise OSError("Cannot overwrite file, as it is set to 'readonly'")
        with open(file) as fr:
            self.overwrite_from_string(fr.read())

    def overwrite_from_string(self, text):
        if self.__readonly:
            raise OSError("Cannot overwrite file, as it is set to 'readonly'")
        yaml.safe_load(text)
        with open(self.path, "w") as fw:
            fw.write(text)

    def export(self, destination):
        with open(self.path) as f:
            with open(destination, "w") as fw:
                fw.write(f.read())


class ProjectConfig(Config):
    pass


class ProgramConfig(Config):
    default_directory = os.path.abspath(
        os.path.join(os.path.dirname(os.path.dirname(__file__)), "data")
    )

    def __init__(self, *args, **kwargs):
        self._readonly = True
        super().__init__(*args, **kwargs)
