import os
import sys
import re
import random
import requests
import urllib
import json
import geojson

import folium
import logging
import simplejson as json
from decimal import Decimal

from folium.plugins import LocateControl

sys.setrecursionlimit(10000)

first_node_name = "0.json"
letters_digits = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
world_boundaries = [Decimal(-180.0), Decimal(-90.0), Decimal(180.0), Decimal(90.0)]
is_a_node_file = lambda fi: re.match(r"\d_[a-z\-]+_\d+\.json", fi)


class Quadtree(object):
    padded_key_idx = 14

    def node_file_basename(level, position, index):
        return f"{level}_{position}_{str(index).rjust(Quadtree.padded_key_idx, '0')}"

    does_point_intersect = (
        lambda pt, bb: bb[0] <= pt[0] <= bb[2] and bb[1] <= pt[1] <= bb[3]
    )

    def does_bbox_fit(pts, bb):
        return sum([Quadtree.does_point_intersect(pt, bb) for pt in pts]) == len(pts)

    current_item = None

    def save_to_html(quadtree_directory, target_directory):
        if not os.path.exists(target_directory):
            os.makedirs(target_directory)
        for fi in os.listdir(quadtree_directory):
            if not fi == first_node_name and not is_a_node_file(fi):
                continue
            with open(os.path.join(quadtree_directory, fi)) as f:
                node_metadata = geojson.load(f)
            bb = node_metadata["bbox"]
            center = [bb[0] + (bb[2] - bb[0]), bb[1]]

            m = folium.Map(location=center, width=750, height=500)
            # LocateControl(auto_start=True).add_to(m)
            for position in node_metadata["children"]:
                child = node_metadata["children"][position]
                filename = child.get("filename")
                if not filename:
                    continue
                bb = child["bbox"]
                m.add_child(
                    folium.vector_layers.Rectangle(
                        [[float(bb[1]), float(bb[0])], [float(bb[3]), float(bb[2])]],
                        fill=1,
                        popup=f"<a href=\"{child['filename'].replace('.json', '.html')}\">Zoom in ({filename})</a>",
                        fill_color="blue",
                    )
                )
            for a in node_metadata["features"]:
                tooltip = ""
                if a.get("name"):
                    tooltip += str(a["name"])
                if a.get("url"):
                    tooltip += "<br>redirects to: " + str(a["url"])
                geo = folium.GeoJson(
                    a["geometry"],
                    tooltip=tooltip,
                    style_function=lambda x: {
                        "color": "red",
                    },
                )
                geo.add_to(m)
            m.save(
                os.path.join(
                    target_directory,
                    (
                        fi.replace(".json", ".html")
                        if not fi == first_node_name
                        else "index.html"
                    ),
                )
            )

    def get_bbox(coordinates):
        polygon_bb = [0.0, 0.0, 0.0, 0.0]
        longs = []
        lats = []
        for polygon in coordinates:
            for lon, lat in polygon:
                longs.append(lon)
                lats.append(lat)
        polygon_bb[0] = min(longs)
        polygon_bb[1] = min(lats)
        polygon_bb[2] = max(longs)
        polygon_bb[3] = max(lats)
        return polygon_bb

    def dump_node_metadata(node_metadata, target_json_file):
        with open(target_json_file, "w") as f:
            json.dump(node_metadata, f, indent=2, ensure_ascii=False)

    def search_node_files(source_directory):
        for r, d, files in os.walk(source_directory):
            for fi in files:
                if not fi == first_node_name and not is_a_node_file(fi):
                    continue
                target = os.path.join(r, fi)
                if not os.path.isfile(target) or not target.endswith(".json"):
                    continue
                yield target

    def search_features_in_node(node_metadata, filter):
        matches = []
        for feature in node_metadata["features"]:
            for i, k in enumerate(filter.keys()):
                if k in feature:
                    if feature[k] == filter[k]:
                        matches.append(i)
                        break
        return matches

    def filter_node_metadata_by_feature(source_directory, filter):
        for node_file in Quadtree.search_node_files(source_directory):
            with open(node_file) as f:
                node_metadata = json.load(f)
            matches = Quadtree.search_features_in_node(node_metadata, filter)
            if matches:
                yield node_file, node_metadata, matches

    def update_features(source_directory, filter, new_data):
        for (
            node_file,
            node_metadata,
            matching_features,
        ) in Quadtree.filter_node_metadata_by_feature(source_directory, filter):
            features = node_metadata.get("features") or []
            for idx in matching_features:
                features[idx] |= new_data
            node_metadata["features"] = features
            Quadtree.dump_node_metadata(node_metadata, node_file)

    def merge(metadata, source_directory, target_directory):
        os.makedirs(target_directory, exist_ok=True)
        for node_file in Quadtree.search_node_files(source_directory):
            with open(node_file) as f:
                node_metadata = json.load(f)
            fi = node_metadata["filename"]
            features = node_metadata.get("features") or []
            for feature in features:
                feature["name"] = metadata["name"]
                feature["id"] = metadata["id"]
                feature["url"] = metadata.get("redirects_to")

            node_metadata["features"] = features
            tfi = os.path.join(target_directory, fi)
            if os.path.exists(tfi):
                with open(tfi) as fr:
                    target_node_metadata = json.load(fr)
                target_features = target_node_metadata.get("features") or []
                target_children = target_node_metadata.get("children") or {}
                if "children" in node_metadata:
                    node_metadata["children"] |= target_children
                else:
                    node_metadata["children"] = target_children
                features_diff = [
                    feature
                    for feature in target_features
                    if not feature["id"] in [fe["id"] for fe in features]
                ]
                node_metadata["features"] += features_diff

            Quadtree.dump_node_metadata(node_metadata, tfi)

    def generate(
        items,
        directory,
        bbox=world_boundaries,
        level=0,
        index=0,
        position="",
        node_min_size=1,
        max_level=1000,
        parent=first_node_name,
    ):
        if level == 0:
            logging.info("Building quadtree...")
            node_file_name = first_node_name
            os.makedirs(directory, exist_ok=True)
            for item in items:
                item["bbox"] = Quadtree.get_bbox(item.geometry.coordinates)
        else:
            node_file_name = (
                Quadtree.node_file_basename(level, position, index) + ".json"
            )

        unsorted_items = items

        node_metadata = {
            "filename": node_file_name,
            "bbox": bbox,
            "children": {},
            "level": level,
            "features": [],
            "parent": parent,
        }

        target_json_file = os.path.join(directory, node_file_name)
        if os.path.exists(target_json_file):
            with open(target_json_file) as f:
                node_metadata = geojson.load(f)
            unsorted_items += node_metadata["features"]

        # If the recursive preset limit is reached,
        # we dump the item reserved to the next generation nodes, into this current node. This will end the recursion at this node level
        if (len(unsorted_items) <= node_min_size) or (
            level >= max_level and max_level > 0
        ):
            node_metadata["features"] = unsorted_items
            Quadtree.dump_node_metadata(node_metadata, target_json_file)
            return node_file_name

        lon_min, lat_min, lon_max, lat_max = bbox

        lon_half = Decimal(lon_max - lon_min) / Decimal(2)
        lat_half = Decimal(lat_max - lat_min) / Decimal(2)

        children_default_values = {
            "bottom-left": {
                "idx": "0",
                "bbox": [
                    lon_min,
                    lat_min,
                    lon_min + lon_half,
                    lat_min + lat_half,
                ],
            },
            "top-left": {
                "idx": "1",
                "bbox": [
                    lon_min,
                    lat_min + lat_half,
                    lon_min + lon_half,
                    lat_max,
                ],
            },
            "top-right": {
                "idx": "2",
                "bbox": [
                    lon_min + lon_half,
                    lat_min + lat_half,
                    lon_max,
                    lat_max,
                ],
            },
            "bottom-right": {
                "idx": "3",
                "bbox": [
                    lon_min + lon_half,
                    lat_min,
                    lon_max,
                    lat_min + lat_half,
                ],
            },
        }
        bboxes_boundary = [
            lon_min - lon_half,
            lat_min - lat_half,
            lon_max + lon_half,
            lat_max + lat_half,
        ]

        children = {}

        uncontained = []
        for item in unsorted_items:
            polygons = item.geometry.coordinates
            polygon_bb = item.get("bbox")
            pbb = [polygon_bb[:2], polygon_bb[2:]]
            does_point_intersect = False
            if Quadtree.does_bbox_fit(pbb, bboxes_boundary):
                for position in children_default_values:
                    child = (children.get(position) or {}) or children_default_values[
                        position
                    ]
                    child["features"] = child.get("features") or []
                    for polygon in polygons:
                        # Polygon at least intersects with the box
                        for pts in polygon:
                            if Quadtree.does_point_intersect(pts, child["bbox"]):
                                child["features"].append(item)
                                # If True, there is at least one boundary that intersects part of this item's geometry
                                does_point_intersect = True
                                break
                    if child["features"]:
                        children[position] = child

            if not does_point_intersect:
                # This is the list of all items that are too big for any of the 4 boundaries (bbox) represented as children of this current node
                uncontained.append(item)

        # We check whether out of the 4 boundaries, only 1 contains all the polygons
        children_features = []
        for position in children:
            children_features += children[position].get("features") or []

        if len(children_features) == 1:
            uncontained += children_features
            #  If this is the case, we can leave the features in this current node.
            children = {}

        ##
        node_metadata["features"] = uncontained
        if not children and not node_metadata["features"]:
            return ""

        children_default_count = 4
        for position, child in children.items():
            child_idx = int(child["idx"])
            child_bbox = child["bbox"]
            child_features = child.get("features")

            if not child_features:
                continue

            next_index = Decimal(index * children_default_count + child_idx)
            next_node_file = Quadtree.generate(
                child_features,
                directory,
                bbox=child_bbox,
                level=level + 1,
                position=position,
                index=next_index,
                node_min_size=node_min_size,
                max_level=max_level,
                parent=node_file_name,
            )
            child["filename"] = next_node_file
            node_metadata["children"][position] = dict(
                [(k, v) for k, v in child.items() if not k == "features"]
            )

        Quadtree.dump_node_metadata(node_metadata, target_json_file)
        if level == 0:
            logging.info("Done building quadtree.")
        return node_file_name


class FeatureCollection(object):
    def __init__(self, _features, name=""):
        self.features = []
        features = geojson.FeatureCollection(_features)
        for feature in features.features:
            feature["name"] = name
            if feature.geometry == None:
                continue
            if feature.geometry.type == "MultiPolygon":
                for coordinates in feature.geometry.coordinates:
                    new_feature = feature.copy()
                    new_feature["geometry"]["type"] = "Polygon"
                    new_feature["geometry"]["coordinates"] = coordinates
                    self.features.append(new_feature)
            else:
                self.features.append(feature)

    def simplify_geometries(self):
        # I tried to use shapely, but the library is very heavy and a little overkill.
        # Other libraries like scikit-geometry are a pain to install, and not available on PyPi.
        # Will ignore this feature for now.
        # for feature in self.features:
        #     geometry = shapely.from_geojson(feature.geometry)
        #     simplified = shapely.simplify(geometry)
        #     feature.geometry = simplified
        pass

    def from_geojson(geojson_file):
        name = os.path.basename(geojson_file).split(".")[-2]
        return FeatureCollection(Geojson.load(geojson_file), name=name)

    def to_geojson(self, geojson_file):
        Geojson.dump(geojson.FeatureCollection(self.features), geojson_file)

    def to_quadtree(self, directory):
        for idx, feat in enumerate(self.features):
            feat["idx"] = idx
        Quadtree.generate(self.features, directory)


class Geojson(object):
    def query_from_openstreetmap(query):
        encoded = urllib.parse.urlencode({"q": query})
        url = f"https://nominatim.openstreetmap.org/search.php?{encoded}&format=jsonv2&polygon_geojson=1"
        logging.info(url)
        response = requests.get(url)
        try:
            results = response.json()
        except requests.exceptions.JSONDecodeError:
            result = response.text
            logging.warning("Unexpected response:\n" + result)
            return result
        else:
            features = []
            for r in results:
                features.append(
                    {
                        "type": "Feature",
                        "id": r["name"],
                        "properties": {"name": r["display_name"]},
                        "geometry": r["geojson"],
                    }
                )
            return geojson.FeatureCollection(features=features)

    def query_from_nominatim(
        lat,
        lon,
        filters=["addresstype", "name", "display_name", "address"],
        zoom="10",  # city
    ):
        bboundaries = []

        url = f"https://nominatim.openstreetmap.org/reverse?lat={lat}&lon={lon}&zoom={zoom}&format=jsonv2&polygon_geojson=1"
        r = requests.get(url)
        try:
            result = r.json()
        except requests.exceptions.JSONDecodeError as e:
            logging.warning(url)
            logging.warning(r.text)
            raise (e)

        features = {
            "type": "Feature",
            "id": result["name"],
            "properties": {"name": result["display_name"]},
            "geometry": result["geojson"],
        }
        return geojson.FeatureCollection(features=features)

    def load(geojson_file):
        with open(geojson_file) as f:
            return geojson.load(f)

    def dump(features, target_file):
        with open(target_file, "w") as f:
            geojson.dump(features, f)

    def parse_directory(geojson_directory):
        geo_files = []
        for root, d, files in os.walk(geojson_directory):
            for fi in files:
                if not fi.endswith(".json") and not fi.endswith(".geojson"):
                    continue
                rfi = os.path.join(root, fi)
                geo_files.append(rfi)
        return geo_files

    def yield_directory(geojson_directory):
        geo_files = Geojson.parse_directory(geojson_directory)
        for rfi in geo_files:
            yield rfi, lambda: FeatureCollection.from_geojson(rfi)

    def save_to_html(features, target, center=[0.0, 0.0, 0.0, 0.0], color="blue"):
        def to_rectangle(c):
            return [[float(_) for _ in c[:2]], [float(_) for _ in c[2:]]]

        m = folium.Map(location=to_rectangle(center)[0], width=750, height=500)
        for ft in features:
            folium.GeoJson(
                ft,
                style_function=lambda feature: {
                    "color": color,
                },
                tooltip=ft.properties.get("name") or "",
            ).add_to(m)
        m.save(target)
