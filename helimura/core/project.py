import logging
import os
import re
import json
from pathlib import Path
import shutil
import sys
import tempfile
import uuid
from datetime import datetime

import gitignorefile
import jinja2

from helimura.core import gis, runtime
from helimura.core.apis import ipfs
from helimura.core.config import ProjectConfig, Metadata

logging.getLogger("root").setLevel(logging.INFO)

backup_ext = ".bkp"


class FileManager(object):
    preset_file = ".preset"
    config_name = ProjectConfig.default_file_name
    project_config_path = ".config"
    content_dirname = "content"
    draft_dirname = "draft"
    release_dirname = "release"
    release_public_name = "0_public"
    release_geofence_name = "1_geofences"
    release_data_name = ""
    publish_related_to = ""

    data_path = ""
    default_project_name = "my-project"
    default_project_directory = runtime.program_project_directory
    template = ".content"
    build_source_dirname = "template"  # os.path.join("template", "jinja")
    dist_dirname = "dist"

    draft_path = os.path.join(content_dirname, draft_dirname)
    draft_dist_path = os.path.join(draft_path, dist_dirname)
    preview_geojson_path = os.path.join(content_dirname, "preview", "geojson")
    preview_geofence_path = os.path.join(content_dirname, "preview", "geofences")
    release_public_path = os.path.join(release_dirname, release_public_name)
    release_data_path = os.path.join(release_dirname, release_data_name)

    @staticmethod
    def merge_folders(src, dest, overwrite=False):
        for r, d, files in os.walk(src):
            dest_subdirectory = Path(dest) / Path(r).relative_to(src)
            if not os.path.exists(dest_subdirectory):
                os.makedirs(dest_subdirectory)

            for fi in files:
                dest_file = os.path.join(dest_subdirectory, fi)
                if os.path.exists(dest_file) and overwrite:
                    os.remove(dest_file)
                if not os.path.exists(dest_file):
                    shutil.copy(os.path.join(r, fi), dest_file)

    @property
    def ipfs_node(self):
        config = runtime.config.load()
        return ipfs.Client.node or config.get("ipfs_node") or ipfs.default_node

    @property
    def ipfs_gateway(self):
        config = runtime.config.load()
        return ipfs.Client.gateway or config.get("ipfs_gateway") or ipfs.default_gateway

    @property
    def config(self):
        return ProjectConfig(directory=self.root_directory, name=self.config_name)

    @property
    def template_directory(self):
        return os.path.join(self._project_template_directory, self.template)

    @property
    def _project(self):
        return Project(self.root_directory)

    def __init__(self, root_directory):
        release_public_path = os.path.join(
            self.release_dirname, self.release_public_name
        )
        self._project_template_directory = runtime.template_directory
        release_data_path = os.path.join(self.release_dirname, self.release_data_name)
        self.root_directory = root_directory
        self.content_directory = os.path.join(self.root_directory, self.content_dirname)
        self.draft_directory = os.path.join(self.content_directory, self.draft_dirname)
        self.build_source_directory = os.path.join(
            self.content_directory, self.build_source_dirname
        )
        self.draft_dist_directory = os.path.join(
            self.draft_directory, self.dist_dirname
        )
        self.build_directory = (
            self.draft_directory
        )  # os.path.join(self.content_directory, self.build_dirname)
        self.release_public_directory = os.path.join(
            self.root_directory, release_public_path
        )
        self.release_data_directory = os.path.join(
            self.root_directory, release_data_path
        )
        self.config_directory = os.path.join(
            self.root_directory, self.project_config_path
        )
        self.release_directory = os.path.join(self.root_directory, self.release_dirname)


class ProjectModule(FileManager):
    template = ".usercontent"

    def __init__(self, root_directory, template=None):
        if template:
            self.template = template
        super().__init__(root_directory)

    def exists(self):
        return True

    def _preset(self, subdirectory="", overwrite=False):
        """Initialize a project's content from a template. Any content already existing in the target directory is not overwritten."""

        src_path = os.path.join(self.template_directory, subdirectory)
        dest_path = os.path.join(self.root_directory, subdirectory)

        FileManager.merge_folders(src_path, dest_path, overwrite=overwrite)

    def preset(self, overwrite=False):
        self._preset(overwrite=overwrite)
        self.build()

    def make(
        self, source_directory=None, target_directory=None, config=None, environ={}
    ):
        target_directory = target_directory or self.build_directory
        source_directory = source_directory or self.build_source_directory

        m = Metadata()
        if not config == None:
            m |= config
        else:
            m |= runtime.config.load()
            m |= self.config.load()
            m |= self._project.metadata.load()
        m |= environ

        e = jinja2.Environment(loader=jinja2.FileSystemLoader(target_directory))
        if not os.path.exists(source_directory):
            return
        for src_r, src_dirs, src_files in os.walk(source_directory):
            if not src_files:
                continue
            target_path = Path(target_directory) / Path(src_r).relative_to(
                source_directory
            )
            os.makedirs(target_path, exist_ok=True)
            for src_file in src_files:
                with open(os.path.join(src_r, src_file)) as f1:
                    target = target_path / Path(src_file)
                    with open(target, "w") as f2:
                        s = e.from_string(
                            f1.read(),
                        )
                        f2.write(s.render(**m.expanded()))

    def build(self):
        # Replace the placeholder variables in the JINJA files, with the actual values found in the metadata file
        config = self.config.load()
        # We replace any value that has Jinja syntax, with its equivalent from environment variables and other user configs
        config.expand(runtime.config.load() | os.environ)
        # and override the existing config file with this update
        config.dump()
        if os.path.exists(self.build_source_directory):
            self.make(
                environ={
                    "HELIMURA_WORKFLOW_STEP": os.getenv("HELIMURA_WORKFLOW_STEP")
                    or "draft"
                },
            )

    def prepublish(self):
        if not self.exists():
            return
        shutil.copytree(
            os.path.join(self.build_directory, self.dist_dirname),
            self.release_public_directory,
            ignore=gitignorefile.ignore(ignore_names=[".release.gitignore"]),
        )
        self.make(
            source_directory=os.path.join(
                self.build_source_directory, self.dist_dirname
            ),
            target_directory=self.release_public_directory,
            environ={
                "HELIMURA_WORKFLOW_STEP": os.getenv("HELIMURA_WORKFLOW_STEP")
                or "release"
            },
        )

        source_m = self.config
        source_m.load()

        # We switch from the project YAML config file set by the user, to a JSON metadata file
        release_m = Metadata(directory=self.release_public_directory)
        # With this line, we strip all variables whose keys are using for code commenting in the YAML file
        # These keys conventionally start with any character other than a letter or a digit
        d = dict(
            [
                (k, source_m.get(k))
                for k in source_m.keys()
                if re.match(r"^[a-zA-Z0-9]", k)
            ]
        )
        release_m |= d
        # This expands all the environment variables found in the metadata file
        release_m.expand()
        release_m.dump()
        logging.info(f"Prepublish: {self.release_public_directory} ready.")

    def publish(self, published_name="", comment="", ipfs_node=None):
        if not self.exists():
            return
        cid_file = "cid.txt"
        source_m = self._project.config
        source_m.load()
        ipfs_node = ipfs_node or self.ipfs_node
        project_name = published_name or source_m.get("name")
        now = datetime.now().strftime("%Y-%m-%d %H:%M:%S")

        relatedto_cid = ""
        if self.publish_related_to:
            with open(
                os.path.join(self.release_directory, self.publish_related_to, cid_file)
            ) as f:
                relatedto_cid = f.read()
        msg = ""
        for dirname in [self.release_public_name, self.release_data_name]:
            if not dirname:
                continue
            src_dir = os.path.join(self.release_directory, dirname)
            with tempfile.TemporaryDirectory() as tmp_dir:
                publish_dir = os.path.join(tmp_dir, dirname)
                shutil.copytree(
                    src_dir,
                    publish_dir,
                    ignore=gitignorefile.ignore(ignore_names=[".publish.gitignore"]),
                )
                publish_name = "%s - %s (%s)" % (now, project_name, dirname)
                published = {"time": now, "comment": comment}
                m = Metadata(directory=src_dir).load()
                m |= {"related_to": relatedto_cid, "published": published}
                m.dump(directory=publish_dir)
                cid = Project.push(
                    publish_dir,
                    name=publish_name,
                    ipfs_node=ipfs_node,
                )
                published |= {"cid": cid}
                m.set("published", published)
                m.dump()
            with open(os.path.join(src_dir, cid_file), "w") as fw:
                fw.write(cid)
            msg += f"new publish: '{publish_name}' \n\
related_to: {relatedto_cid}\n"

        msg += f"ipfs_node: {ipfs_node}\n\
ipfs_gateway: {self.ipfs_gateway}\n"
        logging.info(msg)


class Geofence(ProjectModule):
    name = "geofences"
    template = ".geofences"

    release_public_name = ""
    release_data_name = "2_source"
    publish_related_to = FileManager.release_public_name

    @property
    def config(self):
        return ProjectConfig(directory=self.config_directory, name="geofences.yaml")

    def exists(self):
        config = self.config
        if not config.exists():
            return False
        config.load()
        if not config.keys():
            return False
        return True

    def __init__(self, root_directory):
        super().__init__(root_directory)
        config_value = self._project.config.load().get("downloaded_geofences_quadtree")
        if config_value:
            self.downloaded_geofences_quadtree = os.path.join(
                self.root_directory,
                config_value,
            )
        else:
            self.downloaded_geofences_quadtree = os.path.join(
                self.build_source_directory, self.dist_dirname, self.name, "data"
            )

        self.preview_directory = os.path.join(
            self.root_directory, self.preview_geofence_path
        )

    def reset(self):
        os.remove(self.config.path)
        if os.path.exists(self.preview_directory):
            shutil.rmtree(self.preview_directory)
        if os.path.exists(self.downloaded_geofences_quadtree):
            shutil.rmtree(self.downloaded_geofences_quadtree)
        self.preset(overwrite=True)

    def add_redirect(self, cid, url):
        config = self.config.load()
        d = config.get(cid) or {}
        d |= {"redirects_to": url}
        config.set(cid, d)
        config.dump()
        self.config.read()
        gis.Quadtree.update_features(
            source_directory=self.downloaded_geofences_quadtree,
            filter={"id": d.get("id")},
            new_data={"url": url},
        )

    def download(self, path, gateway=None):
        http, gw, cid_type, source_cid, is_subdomain = ipfs.parse_url(path)
        if not http and not gw:
            # Only a CID was provided, instead of a complete URL. We construct a URL from it
            gw = gateway or self.ipfs_gateway
            http = "https" if is_subdomain else "http"
            if is_subdomain:
                path = f"https://{source_cid}.{cid_type}.{gw}"
            else:
                path = f"{gw}/{cid_type}/{source_cid}"
        geofence_m = Metadata.fetch(path, quiet=True)

        name = geofence_m.get("name")
        id = geofence_m.get("id")
        public_cid = geofence_m.get("related_to")

        if is_subdomain:
            if gateway:
                gw = gateway.replace("http://", "").replace("https://", "")
            public_url = f"{http}{public_cid}.{cid_type}.{gw}"
        else:
            public_url = f"{gateway or (http+gw)}/{cid_type}/{public_cid}"

        source_directory = ipfs.download(path, tempfile.TemporaryDirectory().name)

        config = self.config.load()
        geo_entry = config.get(source_cid) or {}
        geo_entry |= {
            "name": name,
            "id": id,
            "content": public_cid,
            "redirects_to": public_url,
            "downloaded_from": path,
        }

        config.set(source_cid, geo_entry)
        config.dump()

        gis.Quadtree.merge(
            {
                "name": name,
                "id": id,
                "redirects_to": public_url,
            },
            source_directory,
            self.downloaded_geofences_quadtree,
        )
        gis.Quadtree.save_to_html(
            self.downloaded_geofences_quadtree, self.preview_directory
        )
        logging.info("Downloaded geofences from URL " + path)

    def prepublish(self):
        if not self.exists():
            return
        m = self._project.metadata.load()
        m.set("geofences", self.config.load().copy())
        if os.path.exists(self.release_data_directory):
            shutil.rmtree(self.release_data_directory)
        os.makedirs(self.release_data_directory, exist_ok=True)
        m.dump(directory=self.release_data_directory)
        logging.info(f"Prepublish: {self.release_data_directory} ready.")


class Geojson(ProjectModule):
    template = ".geojson"
    geojson_config_path = os.path.join(FileManager.project_config_path, "geojson")
    quadtree_config_path = os.path.join(FileManager.project_config_path, "quadtree")
    release_public_name = ""
    release_data_name = FileManager.release_geofence_name
    publish_related_to = FileManager.release_public_name

    @property
    def metadata(self):
        m = Metadata(directory=self.geojson_config_directory)
        return m

    @property
    def imported(self):
        m = self.metadata
        if not os.path.exists(self.metadata.path):
            return []
        return m.load().get("files") or []

    @property
    def feature_collections(self):
        feature_collections = []
        for gfn in self.imported:
            feature_collections.append(
                gis.FeatureCollection.from_geojson(
                    os.path.join(self.geojson_config_directory, gfn)
                )
            )
        return feature_collections

    @staticmethod
    def yield_geojson_files(geojson_directory):
        for root, d, files in os.walk(geojson_directory):
            for fi in files:
                if not fi.endswith(".json") and not fi.endswith(".geojson"):
                    continue
                rfi = os.path.join(root, fi)
                yield rfi

    def __init__(self, root_directory):
        super().__init__(root_directory)
        self.geojson_config_directory = os.path.join(
            self.root_directory, self.geojson_config_path
        )
        self.preview_directory = os.path.join(
            self.root_directory, self.preview_geojson_path
        )
        self.release_data_directory = os.path.join(
            self.release_directory, self.release_data_name
        )
        self.quadtree_config_directory = os.path.join(
            self.root_directory, self.quadtree_config_path
        )

    def exists(self):
        return bool(self.imported)

    def prepublish(self):
        if not self.exists():
            return
        if os.path.exists(self.release_data_directory):
            shutil.rmtree(self.release_data_directory)

        shutil.copytree(
            self.quadtree_config_directory,
            self.release_data_directory,
            ignore=gitignorefile.ignore(ignore_names=[".quadtree.gitignore"]),
        )
        shutil.copytree(
            self.preview_directory,
            os.path.join(self.release_data_directory, "preview"),
        )
        project_metadata = self._project.metadata.load()
        project_config = self._project.config.load()
        geo_m = Metadata(directory=self.release_data_directory).load()
        geo_m.set("name", project_config.get("name"))
        geo_m.set("id", project_metadata.get("id"))
        geo_m.set(
            "access",
            project_config.get("access"),
        )
        geo_m.dump()
        logging.info(
            f"Prepublish: {self.release_data_directory} ready from {self.template}."
        )

    def from_openstreetmap(self, query, name="", simplify=True):
        result = gis.Geojson.query_from_openstreetmap(query)
        if not result or isinstance(result, str):
            print(result)
            logging.warning("Failed to download GEOJSON")
            return None

        geo_file_name = (result.get("name") or name) + ".geojson"
        tmp_dir = tempfile.TemporaryDirectory()
        geojson_file = os.path.join(tmp_dir.name, geo_file_name)
        gis.Geojson.dump(result, geojson_file)
        self.from_file(geojson_file)
        self.generate_quadtree()
        return result

    def from_txt(self, name, txt):
        with tempfile.TemporaryDirectory() as temp_preview_dir:
            tmp_f = os.path.join(temp_preview_dir, name)
            with open(tmp_f, "w") as f:
                f.write(txt)
            self.from_file(tmp_f)
        self.generate_quadtree()

    def from_file(self, geojson_file, simplify=True):
        geojson_file_name = os.path.basename(geojson_file)
        feature_collection = gis.FeatureCollection.from_geojson(geojson_file)
        feature_collection.to_geojson(
            os.path.join(self.geojson_config_directory, geojson_file_name)
        )

        if geojson_file_name not in self.imported:
            m = self.metadata.load()
            m.set("files", m.get("files") + [geojson_file_name])
            m.dump()
        self.to_html()
        logging.info(f"{geojson_file_name} successfully imported.")
        logging.info("List of geojson:\n" + "\n ".join(self.imported))
        self.generate_quadtree()

    def remove(self, geojson_file_name):
        if not geojson_file_name:
            raise ValueError("You must specify a GEOJSON file name")
        geojson_file_names = self.imported
        if geojson_file_name == "*":
            for rfi in self.imported:
                self.remove(rfi)
        elif geojson_file_name not in geojson_file_names:
            raise ValueError(
                geojson_file_name
                + " not found. List of geojson:\n"
                + "\n ".join(geojson_file_names)
            )
        m = self.metadata.load()
        files = m.get("files") or []
        if geojson_file_name in files:
            files.remove(geojson_file_name)
            logging.info(geojson_file_name + " successfully removed.")
            m.set("files", files)
            m.dump()
        try:
            os.remove(os.path.join(self.geojson_config_directory, geojson_file_name))
        except OSError:
            pass
        finally:
            self.to_html()

    def show(self, geojson_file_name):
        if not geojson_file_name:
            raise ValueError("You must specify a GEOJSON file name")
        target = os.path.join(self.geojson_config_directory, geojson_file_name)
        if not os.path.exists(target):
            raise OSError(
                target + " not found. List of geojson:\n" + "\n ".join(self.imported)
            )
        print(gis.Geojson.load(target))

    def to_html(self):
        content = []
        for feat in self.feature_collections:
            content += feat.features
        gis.Geojson.save_to_html(
            content, os.path.join(self.geojson_config_directory, "index.html")
        )

    def to_quadtree(self):
        for c in self.feature_collections:
            c.to_quadtree(self.quadtree_config_directory)

    def generate_quadtree(self):
        if not self.imported:
            logging.info("This project does not include any geofence.")
        if os.path.exists(self.quadtree_config_directory):
            shutil.rmtree(self.quadtree_config_directory)
        if os.path.exists(self.preview_directory):
            shutil.rmtree(self.preview_directory)
        self.preset()
        self.to_quadtree()
        gis.Quadtree.save_to_html(
            self.quadtree_config_directory, self.preview_directory
        )


class Project(FileManager):
    @property
    def metadata(self):
        return Metadata(directory=self.config_directory)

    @property
    def config(self):
        return ProjectConfig(directory=self.root_directory)

    def generate_id(self):
        metadata = self.metadata.load()
        metadata.set("id", str(uuid.uuid4()))
        metadata.dump()

    def create(self):
        os.makedirs(self.root_directory, exist_ok=True)
        os.umask(0)
        os.chmod(self.root_directory, mode=0o755)
        ProjectModule(self.root_directory).preset()

    def init(self, template, name=""):
        if not os.path.exists(self.root_directory):
            self.create()
            self.generate_id()
        with open(
            os.path.join(
                self._project_template_directory, template, FileManager.preset_file
            )
        ) as f:
            lines = f.read().split("\n")
        for line in lines:
            if not line:
                continue
            ProjectModule(self.root_directory, line).preset(overwrite=True)
        content = ProjectModule(self.root_directory, template)
        content.preset(overwrite=True)
        if name:
            # YAML libraries with python are known for ignoring important stuff such as user comments.
            # So this is a little dirty but we replace the name entry in the text file without altering its content
            config = content.config.load()
            old_name = config.get("name")
            fr = open(config.path)
            buf = fr.read()
            fr.close()
            with open(content.config.path, "w") as fw:
                fw.write(buf.replace(f"name: {old_name}", f"name: {name}"))

        logging.info(
            "New project created to folder %s from template '%s'"
            % (self.root_directory, template)
        )

    def prepublish(self):
        if os.path.exists(self.release_directory):
            shutil.rmtree(self.release_directory)
        Geofence(self.root_directory).prepublish()
        Geojson(self.root_directory).prepublish()
        ProjectModule(self.root_directory).prepublish()

    def publish(
        self, published_name="", comment="", ipfs_node=None, skip_prepublish=False
    ):
        if not skip_prepublish:
            self.prepublish()
        ProjectModule(self.root_directory).publish(published_name, comment, ipfs_node)
        Geofence(self.root_directory).publish(published_name, comment, ipfs_node)
        Geojson(self.root_directory).publish(published_name, comment, ipfs_node)

    @staticmethod
    def push(
        directory,
        name=datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        ipfs_node=None,
    ):
        source_m = Metadata(directory=directory)
        if os.path.exists(source_m.path):
            source_m.load()

        client = ipfs.Client(ipfs_node=ipfs_node)
        result = client.loop.run_until_complete(client.push([directory], name))
        return result

    @staticmethod
    def pull(cid, directory, ipfs_node=None):
        client = ipfs.Client(ipfs_node=ipfs_node)
        return client.loop.run_until_complete(client.pull_directory(cid, directory))

    def print_last_publish_info(self, folder=None):
        if folder is None:
            for dirname in os.listdir(self.release_directory):
                d = os.path.join(self.release_directory, dirname)
                m = Metadata(directory=d).load()
                print(f"{dirname}:{(m.get('published') or {}).get('cid')}")
        else:
            d = os.path.join(self.release_directory, folder)
            m = Metadata(directory=d).load()
            print((m.get("published") or {}).get("cid").rstrip("\r\n"))


def as_config(root_directory):
    return ProjectConfig(directory=root_directory)


def as_content(root_directory):
    return ProjectModule(root_directory)


def as_project(root_directory):
    if root_directory is not None:
        return Project(root_directory)
    else:
        return Project


def as_geojson(root_directory):
    return Geojson(root_directory)


def as_geofence(root_directory):
    return Geofence(root_directory)
