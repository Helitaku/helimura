import os
import sys
import shutil
import re
import argparse
import requests
import tempfile
import io
import tarfile
import logging
import threading
import asyncio
import socket

import aioipfs


default_gateway = "http://127.0.0.1:8080"
default_node = "http://127.0.0.1:5001"


def parse_url(url):
    match = re.match(r"(^http.?://)([a-zA-Z0-9\.\-_:]+)/(i.fs)/([a-z0-9]+)", url)
    if match:
        # returns http, gw, cid_type, source_cid, is_subdomain
        return match[1], match[2], match[3], match[4], False
    else:
        match = re.match(r"(^http.?://)([a-z0-9]+).(i.fs).([a-zA-Z0-9\.\-_:]+)", url)
        if match:
            # returns http, gw, cid_type, source_cid, is_subdomain
            return match[1], match[4], match[3], match[2], True
        else:
            match = re.match(
                r"^(\/?)(ip.s)?(\/?)([a-zA-Z0-9\-]+)$", url
            )  # CID or DNSLink
            if match:
                cid_type = match[2] or "ipfs"
                is_subdomain = cid_type == "ipns"
                # returns http, gw, cid_type, source_cid, is_subdomain
                return None, None, cid_type, match[4], is_subdomain

    logging.error("Couldn't get IPFS gateway and CID from url " + str(url))
    return None, None, None, None, None


class Client(aioipfs.AsyncIPFS):

    node = os.getenv("IPFS_NODE")
    gateway = os.getenv("IPFS_GATEWAY")

    @staticmethod
    def get_maddr(ipfs_node):

        addr_m = re.match(
            r"(https?\:\/\/)?([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3})\:([0-9]+$)",
            ipfs_node or "",
        )
        if addr_m:
            fmt = "ip4"
        else:
            addr_m = re.match(
                r"(https?\:\/\/)?([0-9a-z\-\.]+)\:([0-9]+$)", ipfs_node or ""
            )
            if addr_m:
                fmt = "dns4"
            else:
                return None
        host = socket.gethostbyname(addr_m.group(2))
        scheme = (addr_m.group(1) or "").split(":")[0]
        port = addr_m.group(3)
        return fmt, host, port, scheme

    def __init__(self, ipfs_node=None):
        self.ipfs_node = Client.node or ipfs_node or default_node
        maddr = Client.get_maddr(self.ipfs_node)
        if not maddr:
            raise ValueError("URL for ipfs node is not set.")
        if not len(maddr) == 4:
            raise ValueError(
                "Could not determine ipfs node address from URL " + self.ipfs_node
            )
        fmt, host, port, scheme = maddr
        try:
            loop = asyncio.get_event_loop()
        except RuntimeError:
            loop = asyncio.new_event_loop()
        if port:
            super().__init__(host=host, port=int(port), scheme=scheme, loop=loop)
        else:
            super().__init__(host=host, scheme=scheme, loop=loop)

    # @property
    # def loop(self):
    #     return asyncio.get_event_loop()

    async def add_files(
        self, files, recursive=True, wrap_with_directory=True, to_files=""
    ):
        """
        The add_files function takes a list of files and adds them to the IPFS network.

        Parameters:

            files (list): A list of file paths that will be added to the IPFS network.

            wrap_with_directory (bool, optional): If True, all files will be wrapped in a directory before being added to the IPFS network. Defaults to True.

            recursive (bool, optional): If True, all directories listed in 'files' will have their contents recursively added as well. Defaults to False.

        :param self: Used to Represent the instance of the class.
        :param files: Used to Specify the files to be added to ipfs.
        :param wrap_with_directory=True: Used to Wrap the files in a directory.
        :param to_files="/": Used to Specify the directory where the files will be added.
        :return: The root_cid.

        :doc-author: Trelent
        """
        root_cid = None

        async for added_file in self.add(
            *files,
            to_files="/" + to_files,
            # wrap_with_directory=wrap_with_directory,
            recursive=recursive,
            offline=True,
            cid_version=1
        ):
            logging.debug(added_file)
            root_cid = added_file["Hash"]
        return root_cid

    async def push(self, source: list, name: str):
        """
        The push function takes a source directory and an IPFS node as arguments.
        It then adds the files in the source directory to the IPFS node, and returns
        the hash of that new file.

        :param source_directory: Used to Specify the directory to be added to ipfs.
        :param ipfs_node=None: Used to Specify the ipfs node to use.
        :return: A list of hashes.

        :doc-author: Trelent
        """
        data_to_push = source
        # for s in source:
        #     assert os.path.exists(s)
        #     if os.path.isdir(s):
        #         data_to_push += [os.path.join(s, f) for f in os.listdir(s)]
        #     else:
        #         data_to_push.append(s)
        root_cid = await self.add_files(files=data_to_push, to_files=name)
        await self.close()
        return root_cid

    async def pull_directory(self, cid, target_directory):
        """
        The pull function takes a CID and a target directory as arguments.
        It then downloads the content of the IPFS object with that CID into
        a temporary directory, moves it to the target directory, and deletes
        the temporary directory.

        :param cid: Used to Specify the cid of the file to be pulled.
        :param target_directory: Used to Specify the directory where the files will be downloaded to.
        :param ipfs_node=None: Used to Specify which node to use.
        :return: Nothing.

        :doc-author: Trelent
        """
        if os.path.exists(target_directory):
            raise OSError("path %s exists" % target_directory)
        tmp = tempfile.TemporaryDirectory()
        await self.core.get(cid, dstdir=tmp.name)
        await self.close()
        source_directory = os.path.join(tmp.name, cid)
        shutil.move(source_directory, target_directory)

    async def cat(self, cid):
        _bytes = await self.core.cat(cid)
        await self.close()
        return _bytes


def download(url, target_directory):
    if os.path.exists(target_directory):
        raise OSError("path %s exists" % target_directory)

    response = requests.get(url + "/?format=tar")
    if response.status_code == 200:
        tmp = tempfile.mkdtemp()
        tar = tarfile.open(fileobj=io.BytesIO(response.content))
        tar.extractall(path=tmp)
        source_directory = os.path.join(tmp, url.split("/")[-1])
        shutil.move(source_directory, target_directory)
        shutil.rmtree(tmp)
    else:
        raise requests.ConnectionError(
            "Request to IPFS failed.\nStatus code: %s\n%s"
            % (response.status_code, response.content)
        )
    return target_directory
