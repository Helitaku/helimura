import os
import sys
import argparse
import subprocess as sub
import logging
from pathlib import Path
import nodeenv

# Use this URL for most common OS.
src_base_url = "https://nodejs.org/download/release"
# Or you can use the following URL if you download Node on Alpine Linux
# src_base_url = "https://unofficial-builds.nodejs.org/download/release"
# In any case, is overwritten by the env var "NODEENV_SRC_BASE_URL"
nodeenv.src_base_url = os.getenv("NODEENV_SRC_BASE_URL") or src_base_url

src_url = None

is_from_executable = getattr(sys, "frozen", False)

node_dirname = ".node"
node_directory = os.getenv("NODE_HOME") or os.path.join(
    getattr(sys, "_MEIPASS", os.path.dirname(sys.executable)), node_dirname
)
node_bin_path = "bin"
node_build_path = "build"

if is_from_executable:
    node_directory = node_directory or os.path.join(
        getattr(sys, "_MEIPASS", os.path.dirname(sys.executable)), node_dirname
    )
elif not node_directory:
    logging.warning(
        "'NODE_HOME' not set. Make sure that a node executable is in 'PATH'"
    )

env_path = (os.environ.get("PATH") or "").split(os.pathsep)
node_bin_dir = os.path.join(node_directory, node_bin_path)
if not node_bin_dir in env_path:
    os.environ["PATH"] = os.pathsep.join([node_bin_dir] + env_path)


def install(node_dir, src_dir=None, version=None, clean_src=True):
    """Uses the python library nodeenv to download a version of Node.JS. Then, copies the Node.JS
    executables in the target directory set by variable node_dir
    :param node_dir: target folder for the binary files
    :type node_dir: str
    :param src_dir: temporary install folder for the Node executables, defaults to "src"
    :type src_dir: str, optional
    :param version: Node version to download, defaults to "lts"
    :type version: str, optional
    """
    if not version:
        version = nodeenv.get_last_lts_node_version()
    node_src_dir = src_dir or os.path.join(os.getenv("NODE_HOME"), "node", "src")
    if not os.path.exists(node_dir):
        os.makedirs(node_dir)

    # node_version = nodeenv.get_last_stable_node_version()
    args = argparse.Namespace()
    args.npm = "lts"
    args.no_npm_clean = False
    args.clean_src = clean_src
    args.prebuilt = True
    args.with_npm = True
    args.verbose = True

    args.node = version
    # nodeenv.download_node_src(node.src_url, src_dir, args)
    # nodeenv.copy_node_from_prebuilt(env_dir, src_dir, args.node)
    nodeenv.install_node(node_dir, node_src_dir, args)


def __run(cmd, args, from_dir=None):
    if not from_dir:
        root = os.path.dirname(sys.executable)
        if is_from_executable and not Path(os.getcwd()).is_relative_to(root):
            raise OSError("This command can not be called outside of" + root)

    completed_process = sub.run(cmd + args, capture_output=True, env=os.environ)
    stdout = completed_process.stdout.decode("utf8")
    stderr = completed_process.stderr.decode("utf8")
    if not completed_process.returncode == 0 and stderr:
        raise Exception(stderr)
    return stdout, stderr


def run(cmd, args, target_dir):
    cwd = os.getcwd()
    if target_dir:
        os.chdir(target_dir)
    try:
        stdout, stderr = __run(cmd, args, from_dir=target_dir)
    except Exception as e:
        raise (e)
    else:
        logging.info(stdout)
        return stdout, stderr
    finally:
        if target_dir:
            os.chdir(cwd)


class npm(object):
    def install(args=[], run_in_directory=None):
        cmd = ["npm", "install"]
        return run(cmd, args, run_in_directory)

    def run(args=[], run_in_directory=None):
        cmd = ["npm", "run"]
        return run(cmd, args, run_in_directory)


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("choices", choices=["install"])
    parser.add_argument("directory", type=str)
    args = parser.parse_args()
    if args.choices == "install":
        install(args.directory)
