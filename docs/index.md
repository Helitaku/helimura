"""
## [Return to main website](./index.html)  
# Welcome to Helimura documentation!

Helimura is a framework helping artists to share their digital work in geofenced areas, in the real world.  
Their artwork is hosted on the decentralized network IPFS and any visitor owning a smartphone with geolocation feature, can access it from their preferred web browser, provided they are within the borders of the geofence.
"""
