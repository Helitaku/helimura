#!/bin/sh

# To be launched in the root folder for this project
pip install pdoc mkdocs mkdocs-material
root=`dirname "$0"`
cd $root/..
mkdocs build 
pdoc . -o ./public
