#!/bin/bash -x
world='{
  "type": "FeatureCollection",
  "features": [
    {
      "type": "Feature",
      "geometry": {
        "type": "Polygon",
        "coordinates": [
          [
            [
              -180,
              -90
            ],
            [
              -180,
              90
            ],
            [
              180,
              90
            ],
            [
              180,
              -90
            ],
            [
              -180,
              -90
            ]
          ]
        ]
      },
      "properties": {
        "name": "World"
      }
    }
  ]
}'

if [[ "$HELIMURA_USE_SOURCE" ]]; then
    cmd="python -m helimura"
else
  if [[ "$PIPENV_ACTIVE" ]]; then
    cmd="python -m helimura"
  else
    cmd="helimura"
    fi
fi

# if [[ ! "$HELIMURA_WORKFLOW_STEP" ]]; then
#   export HELIMURA_WORKFLOW_STEP=release
# fi
if [[ ! "$HELIMURA_HOME" ]]; then
  HELIMURA_HOME=$HOME/.helimura/projects
fi
if [[ ! "$DEFAULT_ACCESS_URL" ]]; then
  DEFAULT_ACCESS_URL=http://127.0.0.1:8080
fi
rm -r $HELIMURA_HOME/project.artwork
$cmd init webpage --name 'project.artwork'
rm -r $HELIMURA_HOME/project.artwork
# $cmd init webpage --name 'demo-artwork' $HELIMURA_HOME/project.artwork
$cmd init webpage $HELIMURA_HOME/project.artwork
# $cmd init threejs --name 'demo-artwork' $HELIMURA_HOME/project.artwork

tmp=`mktemp`
echo $world > $tmp
$cmd geojson add --name 'world' --fromfile  $tmp $HELIMURA_HOME/project.artwork
$cmd geojson list $HELIMURA_HOME/project.artwork
# First time access add is called, access mode will switch to 'restricted'. To undo this mode, you will need to set it manually
# in 'settings.yaml'
$cmd access add --mode "draft" "$DEFAULT_ACCESS_URL" $HELIMURA_HOME/project.artwork
$cmd access add "$DEFAULT_ACCESS_URL" $HELIMURA_HOME/project.artwork
$cmd access add "https://*.ipfs.io" $HELIMURA_HOME/project.artwork
$cmd access add "https://ipfs.io" $HELIMURA_HOME/project.artwork
$cmd access list $HELIMURA_HOME/project.artwork
$cmd build $HELIMURA_HOME/project.artwork
if [[ ! "$IPFS_NODE" ]]; then
$cmd publish $HELIMURA_HOME/project.artwork
else
$cmd publish --ipfs_node $IPFS_NODE $HELIMURA_HOME/project.artwork
fi

# cid=`cat $HELIMURA_HOME/project.artwork/release/1_geofences/cid.txt`
cid=`$cmd publish --last 1_geofences $HELIMURA_HOME/project.artwork | sed 's/\r//g'`
redirect_url=$DEFAULT_ACCESS_URL/ipfs/`$cmd publish --last 0_public $HELIMURA_HOME/project.artwork | sed 's/\r//g'`
rm -r $HELIMURA_HOME/project.gallery

# $cmd init gallery --name 'demo-gallery' $HELIMURA_HOME/project.gallery
$cmd init gallery $HELIMURA_HOME/project.gallery
$cmd geofence download $cid $HELIMURA_HOME/project.gallery
$cmd geofence redirect $redirect_url $cid $HELIMURA_HOME/project.gallery
$cmd geofence list $HELIMURA_HOME/project.gallery
$cmd build $HELIMURA_HOME/project.gallery
if [[ ! "$IPFS_NODE" ]]; then
$cmd publish $HELIMURA_HOME/project.gallery
else
$cmd publish --ipfs_node $IPFS_NODE $HELIMURA_HOME/project.gallery
fi

echo "Open $DEFAULT_ACCESS_URL/ipfs/`cat $HELIMURA_HOME/project.gallery/release/0_public/cid.txt` to get started"
