docker rm ipfs
docker network create --attachable=true ipfs
docker run -d --name ipfs -e IPFS_PATH=/data/ipfs -p 5001:5001 -p 8080:8080 --network ipfs -v ipfs_path:/data/ipfs ipfs/kubo
docker run --rm -ti -v $(pwd)/requirements.txt:/content/requirements.txt -v $(pwd)/.content:/content -p 3000:3000 --network=host helimura bash
docker run --rm -ti --entrypoint="" -v /tmp/output:/tmp/output -v $(pwd):/src:ro --network=host pyinstaller-windows bash
